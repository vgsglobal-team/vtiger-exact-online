<?php
/*
class Exactonline_List_View extends Vtiger_Index_View {

    public function process(Vtiger_Request $request) {
        $viewer = $this->getViewer($request);
        $viewer->view('Index.tpl', $request->getModule());
    }
}*/

//require_once 'modules/Exactonline/libs/EOL_API.php';
include_once 'modules/Exactonline/libs/ExactOnLineFunctions.php';

class Exactonline_List_View extends Vtiger_Index_View {
    
    	function checkPermission(Vtiger_Request $request) {
               $currentUser = Users_Record_Model::getCurrentUserModel();
               if(!$currentUser->isAdminUser()){
                   throw new AppException(vtranslate($moduleName).' '.vtranslate('LBL_NOT_ACCESSIBLE'));  
               }
	}

    public function process(Vtiger_Request $request) {
        global $site_URL;
        $configOptions = $this->getConfigOptions();

        $viewer = $this->getViewer($request);
        foreach($configOptions as $var => $value){
            $viewer->assign($var,$value);
        }
        //Get the auth code and save it in the databse

        if($request->get('code') != ''){
           ExactOnlineApiWrapper::setValue('authorizationcode', $request->get('code'));
        }

        $viewer->assign("action_url",$site_URL."index.php?module=Exactonline&action=Saveconfig");
        $viewer->view('Index.tpl', $request->getModule());
    }

    function updateAccessToken($accessToken){
        $this->updateConfig("access_token",$accessToken->access_token);
        $this->updateConfig("refresh_token",$accessToken->refresh_token);
    }

    function getConfigOptions(){
        global $adb;
        $configQuery = $adb->pquery("select var,value from vtiger_exact_config",array());
        $queryCount = $adb->num_rows($configQuery);
        $queryResult = array();
        for($i = 0;$i<$queryCount;$i++){
            $queryResult[$adb->query_result($configQuery,$i,'var')] = $adb->query_result($configQuery,$i,'value');
        }
        return $queryResult;
    }

    function updateConfig($var,$value){
        global $adb;
        $configQuery = $adb->pquery("select value from vtiger_exact_config where var = ?",array($var));
        $configValue = $adb->query_result($configQuery,0,'value');
        if($adb->num_rows($configQuery) != 0)
            $adb->pquery("update vtiger_exact_config set value = ? where var = ?",array($value,$var));
        else
            $adb->pquery("insert into vtiger_exact_config (var,value) values (?,?)",array($var,$value));
    }

}