<?php

require 'modules/Exactonline/utils/ExactUtils.php';
//error_reporting(E_ALL);ini_set('display_errors','On');

class Exactonline_EditConfig_View extends Vtiger_Index_View {
    public function process(Vtiger_Request $request) {
        global $site_URL;
        $viewer = $this->getViewer($request);
        $exactUtils = new ExactUtils();

        $fieldSyncOptions = $exactUtils->syncFieldsOptions();
        $viewer->assign("fieldSyncOptions",$fieldSyncOptions);

        $configuredValues = $exactUtils->getConfiguredValues();
        foreach($configuredValues as $var => $value){
            $viewer->assign($var,$value);
        }
    //mapping users
        $viewer->assign("vtigerUsers",$exactUtils->getVtigerUsers());
        $viewer->assign("eolUsers",$exactUtils->getEOLUsers());

    //mapping account,product, salesorderfields
        $viewer->assign('accountFields',$exactUtils->getAccountFields());
        $viewer->assign('salesOrderFields',$exactUtils->getSalesOrderFields());
        $viewer->assign("PRODUCTMODFIELDS",$exactUtils->getProductFields());

    //Assign Classification
        $viewer->assign('CLASSIFICATION',$exactUtils->getEOLAccountsClassification());
        $viewer->assign('ACCOUNTTYPE',$exactUtils->getAccountType());

    //Assign Warehouse
        $viewer->assign("WAREHOUSES",$exactUtils->getEOLWarehouses());

    //salesorder config
        $viewer->assign("VATCODES",$exactUtils->getEOLVatCodes());
        $viewer->assign("COSTCENTERS",$exactUtils->getEOLCostCenters());

    //invoice sending method
        $invoiceSendingMethod = $exactUtils->getInvoiceSendingMethod();
        $viewer->assign("sendingMethod",$invoiceSendingMethod);
        
    //Payment Conditions
        $paymentConditions = $exactUtils->getPaymentConditions();
        $viewer->assign("paymentConditions",$paymentConditions);
        
    //Payment Conditions (status EOL)
        $SOStatus = $exactUtils->getSOStatus();
        $viewer->assign("salesOrderStatus",$SOStatus);

    //Payment Conditions (status VT)
        $VTSOStatus = $exactUtils->getVTSOStatus();
        $viewer->assign("salesOrderStatusVT",$VTSOStatus);
        
    //VT Products Categories
        $VTProdCat = $exactUtils->getVTProdCat();
        $viewer->assign("productCategories",$VTProdCat);
        
        $viewer->assign("action_url",$site_URL."index.php?module=Exactonline&action=Saveconfiguration");
        $viewer->view('EditConfig.tpl', $request->getModule());
    }
}