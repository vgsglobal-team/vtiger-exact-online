<?php
include_once 'modules/Exactonline/libs/ExactOnLineFunctions.php';

class Exactonline_Saveconfig_Action extends Vtiger_Save_Action {

    public function process(Vtiger_Request $request) {
        global $site_URL;
        

        if ($request->get('isrevoked')) {
            $this->deleteOldCode();
        } else {
            $fields = array('clientid', 'clientsecret', 'division');
            
            foreach ($fields as $field) {
                $this->updateConfig($field, $request->get($field));
            }

            if (!ExactOnlineApiWrapper::getValue('authorizationcode')) {
                $EOLApi = new ExactOnlineApiWrapper();
                $EOLApi->authorize();
            }
        }




        $redirectURL = $site_URL . "index.php?module=Exactonline&view=Credentail";
        header("Location: $redirectURL");
    }

    function checkPermission(Vtiger_Request $request) {
        $moduleName = $request->getModule();
        $moduleModel = Vtiger_Module_Model::getInstance($moduleName);

        $currentUserPriviligesModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
        if (!$currentUserPriviligesModel->hasModulePermission($moduleModel->getId())) {
            throw new AppException(vtranslate($moduleName) . ' ' . vtranslate('LBL_NOT_ACCESSIBLE'));
        }
    }

    function deleteOldCode() {
        global $adb;
        $fields = "('authorizationcode','clientid', 'clientsecret', 'division')";
        $adb->pquery("DELETE FROM vtiger_exact_config WHERE var IN $fields", array());
        Vtiger_Cache::flush();
    }

    function updateConfig($var, $value) {
        global $adb;
        $configQuery = $adb->pquery("select value from vtiger_exact_config where var = ?", array($var));
        $configValue = $adb->query_result($configQuery, 0, 'value');
        if ($adb->num_rows($configQuery) != 0)
            $adb->pquery("update vtiger_exact_config set value = ? where var = ?", array($value, $var));
        else
            $adb->pquery("insert into vtiger_exact_config (var,value) values (?,?)", array($var, $value));
    }

}
