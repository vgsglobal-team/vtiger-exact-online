<?php

class Exactonline_TryConnectionsAjax_Action extends Vtiger_BasicAjax_Action {

    function __construct() {
        parent::__construct();
    }

    function process(Vtiger_Request $request) {
        
        require_once 'modules/Exactonline/utils/ExactUtils.php';
        require_once 'modules/Exactonline/utils/CommonUtils.php';
        
        $aux = new ExactOnlineApiWrapper();
        $this->eol_api = $aux->connect();

        if ($this->eol_api) {
            $conn_response['status'] = 'ok';
            $conn_response['message'] = getTranslatedString('LBL_CONNECTION_WOKS', 'VGSWooSynch');
        } else {
            $conn_response['status'] = 'fail';
            $conn_response['message'] = getTranslatedString('LBL_CONNECTION_FAILED', 'VGSWooSynch');
        }

        $response = new Vtiger_Response();
        $response->setResult($conn_response);
        $response->emit();
    }

}

?>