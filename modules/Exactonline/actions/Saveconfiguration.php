<?php

require "modules/Exactonline/ConfigHelper.php";

class ExactOnline_Saveconfiguration_Action extends Vtiger_Save_Action {
    
    public function checkPermission(Vtiger_Request $request) {
        $currentUser = Users_Record_Model::getCurrentUserModel();
        if($currentUser->isAdminUser()){
            return true;
        }  else {
            throw new AppException('LBL_PERMISSION_DENIED');
        }
    }

    public function process(Vtiger_Request $request){
        global $site_URL;
        $formValues = $request->getAll();
        $checkboxFields = array("salesorder_map_cvalues4pc","salesorder_map_pullstatusfromeol","salesorder_map_pushstatustoeol","invoice_syncinterval","product_get_stock_count","product_get_reorder_level","product_map_pull_categories_from_eol","product_map_push_categories_to_eol");
        foreach($checkboxFields as $val){
            if (!array_key_exists($val, $formValues)) {
                $formValues[$val] = "off";
            }
        }
        $dontSave = array("module","action","__vtrftk");
        foreach($formValues as $key => $value){
            if(!in_array($key,$dontSave))
                $this->updateConfig($key,$value);
        }
        $redirectURL = $site_URL."index.php?module=Exactonline&view=Config";
        header("Location: $redirectURL");
    }


    function updateConfig($var,$value){
        global $adb;
        $configQuery = $adb->pquery("select value from vtiger_exact_config where var = ?",array($var));
        $configValue = $adb->query_result($configQuery,0,'value');
        if($adb->num_rows($configQuery) != 0)
            $adb->pquery("update vtiger_exact_config set value = ? where var = ?",array($value,$var));
        else
            $adb->pquery("insert into vtiger_exact_config (var,value) values (?,?)",array($var,$value));
        return true;
    }
}

