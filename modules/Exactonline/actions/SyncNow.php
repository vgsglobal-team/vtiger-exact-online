<?php

class Exactonline_SyncNow_Action extends Vtiger_BasicAjax_Action {

    function __construct() {
        parent::__construct();
    }

    function process(Vtiger_Request $request) {
        
        require_once 'modules/Exactonline/utils/AccountManager.php';
        $EOLAccountManager = new Exactonline_AccountManager();
        $EOLAccountManager->SyncAccounts();

        require_once 'modules/Exactonline/utils/ProductsManager.php';
        $EOLProductManager = new Exactonline_ProductManager();
        //$EOLProductManager->SyncProducts();
        
        require_once 'modules/Exactonline/utils/SalesOrderManager.php';
        $EOLSalesOrderManager = new Exactonline_SalesOrderManager();
        //$EOLSalesOrderManager->SyncSalesOrder();
        
        $response = new Vtiger_Response();
        $response->setResult('Synch completed');
        $response->emit();
    }
}
?>