<?php

require_once("modules/Exactonline/EventHandler.php");

class Exactonline_ActionAjax_Action extends Vtiger_IndexAjax_View {

        const INITIAL = "Initialised";
        const DOWNLOADING = "Downloading";
        const DOWNLOADED = "Downloaded";
        const DOWNLOADFAILED = "Download Failed";
        const PROCESSING = "Processing";
        const PROCESSED = "Processed";

        public function process(Vtiger_Request $request) {
            $mode = $request->get('mode');
            $responseResult = $this->$mode($request);
            $response = new Vtiger_Response();
            $response->setResult($responseResult);
            $response->emit();
         }

	public function updateLogForEvent($request){
            global $adb;
            $eventId = $request->get('eventid');	
            $eventQuery = $adb->pquery("select state,updated_time,synctype from vtiger_exact_syncevent where synceventid = ?",array($eventId));
            $syncType = $adb->query_result($eventQuery,0,'synctype');
            $state = $adb->query_result($eventQuery,0,'state');
            $updatedTime = $adb->query_result($eventQuery,0,'updated_time');
            $module = $this->getModuleForSyncType($syncType);	
            $sendSyncTypes = array("send_contacts","send_accounts","send_invoice","send_salesorder","send_products");
            if(!in_array($syncType,$sendSyncTypes)){
                $eventStatus = "Event last updated on $updatedTime and its current state is $state.<br>";
                $downloadStatusQuery = $adb->pquery("select count(downloadid) as downloadcount,sum(processingindex) as processedcount from vtiger_exact_download where synceventid = ?",array($eventId));
                $downloadCount = $adb->query_result($downloadStatusQuery,0,'downloadcount');
                if($downloadCount > 0)
                    $eventStatus .= "Downloaded $downloadCount number of xmls, with each 60 records.<br>";
                $processedCount = $adb->query_result($downloadStatusQuery,0,'processedcount');
                if($processedCount > 0)
                    $eventStatus .= "So far process $processedCount number of accounts.<br>";
            }else{
                $eventStatus = "";
                $logQuery = $adb->pquery("select * from vtiger_exact_synceventlog where synceventid = ? order by synceventlogid desc",array($eventId));
                $exceptionCount = $adb->num_rows($logQuery);
                if($exceptionCount){
                    $eventStatus .= "So far count of exceptions raised in this sync is $exceptionCount<br><br>";
                    $eventStatus .= "Last exception message at ".$adb->query_result($logQuery,0,'created_time')." is ".$adb->query_result($logQuery,0,'message')."<br><br>";
                    $eventStatus .= "Last response for the exception is ".$adb->query_result($logQuery,0,'response')."<br><br>";
                }else{
                    $eventStatus .= "Event last updated on $updatedTime and its current state is $state.<br>";
                }
            }
            return array("responseMessage" => $eventStatus);
	}
	
	public function handleEvent($request){
            $eventId = $request->get("eventid");
            $eventHandler = new EventHandler($eventId);
            return array("responseMessage"=>"Initiating Event");
	}

	public function processEvent($eventId){
            $eventHandler = new EventHandler($eventId);
	}

	//called when clicking buttons in manual sync page
	public function registerEvent($request){
            $processLastMinutes = 10;
            $event = $request->get('transferEvent');
            $module = $this->getModuleForSyncType($event);
            $eventStatus = $this->checkEventStatus($event);
            if(is_array($eventStatus)){
                $eventId = $eventStatus['eventId'];
                $eventCreatedTime = $eventStatus['created_time'];
                $state = $eventStatus['state'];
                $updatedTime = $eventStatus['updated_time'];
                $now = time();
                $updatedTimeStamp = strtotime($updatedTime);
                $diffMinutes = floor(($now - $updatedTimeStamp)/60);
                $reinitiateAgain = '';
                if($diffMinutes > $processLastMinutes){
                    $reinitiateAgain .= "<br> The process is reinitiating";
                    $return ["isInitialised"] = true;
                }
                $return['eventid'] = $eventId;
                $return['responseMessage'] = "Getting $module is already created at $eventCreatedTime and last updated at $updatedTime".$reinitiateAgain;
                return $return;
            }
            if(!$eventStatus){
                return $this->initialiseEvent($event);
            }
            //get event id and update its status in responseMessage
            $eventId = $eventStatus;
            $responseMessage = "Getting $module already initialised";
            return array('eventid' => $eventId, 'responseMessage' => $responseMessage);
	}

	public function initialiseEvent($event){
            global $adb;
            $eventInsertQuery = $adb->pquery("insert into vtiger_exact_syncevent (synctype,state,message,created_time,updated_time) values (?,?,?,?,?)",array($event,self::INITIAL,"Initialised",date("Y-m-d h:i:s"),date("Y-m-d h:i:s")));
            $eventId = $adb->getLastInsertID();
            return array("eventid" => $eventId,"responseMessage"=>"Event Initialised successfully","isInitialised" => true);
	}

	//TODO: return whethere the event can be initialised or not
	public function checkEventStatus($event){
            global $adb;
            $alreadyInitialisedQuery = $adb->pquery("select synceventid,created_time,state,updated_time from vtiger_exact_syncevent where synctype = ? and state in ('Initialised','Downloading','Processing','Downloaded') order by synceventid asc limit 1",array($event));
            if($adb->num_rows($alreadyInitialisedQuery)!=0)
                    return array("eventId"=>$adb->query_result($alreadyInitialisedQuery,0,'synceventid'),"created_time"=>$adb->query_result($alreadyInitialisedQuery,0,'created_time'),"state"=>$adb->query_result($alreadyInitialisedQuery,0,'state'), "updated_time"=> $adb->query_result($alreadyInitialisedQuery,0,'updated_time'));
            return false;
	}

        public function getModuleForSyncType($syncType){
            if($syncType == "get_accounts")
                return "Accounts";
            if($syncType == "get_products")
                return "Items";
            if($syncType == "get_salesorder")
                return "SalesOrders";
            if($syncType == "get_invoice")
                return "Invoices";
        }
}
