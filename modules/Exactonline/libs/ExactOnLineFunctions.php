<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require __DIR__ . '/vendor/autoload.php';
require_once 'modules/Exactonline/utils/CommonUtils.php';
include_once 'include/utils/utils.php';
//include_once 'modules/Exactonline/utils/CommonUtils.php';

class ExactOnlineApiWrapper {

    private $exactClientId;
    private $exactClientSecret;
    private $redirectUrl;

    function __construct() {
        global $site_URL;
        //Need to load the information from the database and set the constants

        $this->exactClientId = getConfigValue('clientid');
        $this->exactClientSecret = getConfigValue('clientsecret');

        if (substr($site_URL, -1) != '/') {
            $site_URL = $site_URL . '/';
        }

        $this->redirectUrl = $site_URL . 'index.php?module=Exactonline&view=Credentail';
    }

    /**
     * Function to retrieve persisted data for the example
     * @param string $key
     * @return null|string
     */
    public static function getValue($key) {
        if(getConfigValue($key)){
            return getConfigValue($key);
        }
        return null;
    }

    public static function setValue($key, $value) {
        updateConfig($key, $value);
    }

    function connect() {
        $connection = new \Picqer\Financials\Exact\Connection();
        $connection->setRedirectUrl($this->redirectUrl);
        $connection->setExactClientId($this->exactClientId);
        $connection->setExactClientSecret($this->exactClientSecret);

        if ($this->getValue('authorizationcode')) { // Retrieves authorizationcode from database
            $connection->setAuthorizationCode($this->getValue('authorizationcode'));
        }

        if ($this->getValue('accesstoken')) { // Retrieves accesstoken from database
            $connection->setAccessToken($this->getValue('accesstoken'));
        }

        if ($this->getValue('refreshtoken')) { // Retrieves refreshtoken from database
            $connection->setRefreshToken($this->getValue('refreshtoken'));
        }

        // Make the client connect and exchange tokens
        try {
            $connection->client();
        } catch (\Exception $e) {
            throw new Exception('Could not connect to Exact: ' . $e->getMessage());
        }

        // Save the new tokens for next connections
        $this->setValue('accesstoken', $connection->getAccessToken());
        $this->setValue('refreshtoken', $connection->getRefreshToken());

        return $connection;
    }

    function authorize() {
        $connection = new \Picqer\Financials\Exact\Connection();
        $connection->setRedirectUrl($this->redirectUrl);
        $connection->setExactClientId($this->exactClientId);
        $connection->setExactClientSecret($this->exactClientSecret);
        $connection->redirectForAuthorization();
    } 
}
