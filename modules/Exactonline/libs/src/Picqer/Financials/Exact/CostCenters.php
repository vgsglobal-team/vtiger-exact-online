<?php namespace Picqer\Financials\Exact;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CostCenters extends Model
{

    use Query\Findable;

    protected $fillable = [
        'ID',
        'Active',
        'Code',
        'Division',
        'Description',
    ];

    protected $url = 'hrm/Costcenters';

}