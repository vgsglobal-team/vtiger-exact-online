<?php

namespace Picqer\Financials\Exact;

class Warehouses extends Model {

    use Query\Findable;

    protected $fillable = [
        'UserID',
        'BirthDate',
        'BirthName',
        'Code',
        'Created',
        'Creator',
        'CreatorFullName',
        'Description',
        'EMail',
        'Main',
        'ManagerUser',
        'UseStorageLocations',
        'Division',
        'Modified',
        'Modifier',
        'ModifierFullName',
    ];
    
    protected $url = 'inventory/Warehouses';

}
