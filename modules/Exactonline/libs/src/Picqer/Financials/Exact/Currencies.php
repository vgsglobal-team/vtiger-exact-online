<?php namespace Picqer\Financials\Exact;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Currencies extends Model
{

    use Query\Findable;

    protected $fillable = [
        'Code',
        'Description',
    ];

    protected $url = 'general/Currencies';

}