<?php

namespace Picqer\Financials\Exact;

class Users extends Model {

    use Query\Findable;

    protected $fillable = [
        'UserID',
        'BirthDate',
        'BirthName',
        'Code',
        'Created',
        'Creator',
        'CreatorFullName',
        'Customer',
        'CustomerName',
        'Email',
        'EndDate',
        'FirstName',
        'FullName',
        'Gender',
        'Initials',
        'Language',
        'LastLogin',
        'LastName',
        'MiddleName',
        'Mobile',
        'Modified',
        'Modifier',
        'ModifierFullName',
        'Nationality',
        'Notes',
        'Phone',
        'PhoneExtension',
        'ProfileCode',
        'StartDate',
        'StartDivision',
        'Title',
        'UserName',
        'UserRoles',
    ];
    
    protected $url = 'users/Users';

}
