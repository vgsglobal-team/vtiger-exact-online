<?php

// Autoload composer installed libraries
require __DIR__ . '/../vendor/autoload.php';

ini_set('display_errors', 'on');

/**
 * Function to retrieve persisted data for the example
 * @param string $key
 * @return null|string
 */
function getValue($key) {
    $storage = json_decode(file_get_contents('storage.json'), true);
    if (array_key_exists($key, $storage)) {
        return $storage[$key];
    }
    return null;
}

/**
 * Function to persist some data for the example
 * @param string $key
 * @param string $value
 */
function setValue($key, $value) {
    $storage = json_decode(file_get_contents('storage.json'), true);
    $storage[$key] = $value;
    file_put_contents('storage.json', json_encode($storage));
}

/**
 * Function to authorize with Exact, this redirects to Exact login promt and retrieves authorization code
 * to set up requests for oAuth tokens
 */
function authorize() {
    $connection = new \Picqer\Financials\Exact\Connection();
    $connection->setRedirectUrl('http://localhost:8888/desarrollos/exact-php-client-v1/example/example.php');
    $connection->setExactClientId('8c590760-f353-462c-a978-70d781ee7196');
    $connection->setExactClientSecret('w4V88KXRDBbm');
    $connection->redirectForAuthorization();
}

/**
 * Function to connect to Exact, this creates the client and automatically retrieves oAuth tokens if needed
 *
 * @return \Picqer\Financials\Exact\Connection
 * @throws Exception
 */
function connect() {
    $connection = new \Picqer\Financials\Exact\Connection();
    $connection->setRedirectUrl('http://localhost:8888/desarrollos/exact-php-client-v1/example/example.php');
    $connection->setExactClientId('8c590760-f353-462c-a978-70d781ee7196');
    $connection->setExactClientSecret('w4V88KXRDBbm');

    if (getValue('authorizationcode')) { // Retrieves authorizationcode from database
        $connection->setAuthorizationCode(getValue('authorizationcode'));
    }

    if (getValue('accesstoken')) { // Retrieves accesstoken from database
        $connection->setAccessToken(getValue('accesstoken'));
    }

    if (getValue('refreshtoken')) { // Retrieves refreshtoken from database
        $connection->setRefreshToken(getValue('refreshtoken'));
    }

    // Make the client connect and exchange tokens
    try {
        $connection->client();
    } catch (\Exception $e) {
        throw new Exception('Could not connect to Exact: ' . $e->getMessage());
    }

    // Save the new tokens for next connections
    setValue('accesstoken', $connection->getAccessToken());
    setValue('refreshtoken', $connection->getRefreshToken());

    return $connection;
}

// If authorization code is returned from Exact, save this to use for token request
if (isset($_GET['code']) && is_null(getValue('authorizationcode'))) {
    setValue('authorizationcode', $_GET['code']);
}

// If we do not have a authorization code, authorize first to setup tokens
if (getValue('authorizationcode') === null) {
    authorize();
}

// Create the Exact client
$connection = connect();


/* Devuelve la lista de accounts filtradas por modified date */

//try {
//    $accounts = new \Picqer\Financials\Exact\Account($connection);
//    
//
//    
//    $lastSyncDate = '2001-05-29 09:13:28'; //  -> Esto tiene que ser la fecha que se corrio la interfaz por ultima vez. Desde el log!!
//    $lastSyncDate = date('Y-m-d\Th:i:s', strtotime($lastSyncDate));
//    
//    $lastSyncDate = "DateTime'$lastSyncDate'" ;
//    
//    $result   = $accounts->filter('Modified gt ' .$lastSyncDate);
//    foreach ($result as $account) {
//        var_dump($account->attributes());
//        echo '<br>';
//    }
//} catch (\Exception $e) {
//    echo get_class($e) . ' : ' . $e->getMessage();
//}


/* Devuelve la lista de Productos filtradas por modified date */

//try {
//    $products = new \Picqer\Financials\Exact\Item($connection);
//    
//    $lastSyncDate = '2001-05-29 09:13:28'; //  -> Esto tiene que ser la fecha que se corrio la interfaz por ultima vez. Desde el log!!
//    $lastSyncDate = date('Y-m-d\Th:i:s', strtotime($lastSyncDate));
//    
//    $lastSyncDate = "DateTime'$lastSyncDate'" ;
//    
//    //$result   = $products->filter('Modified gt ' .$lastSyncDate);
//    foreach ($result as $product) {
//         var_dump($product->attributes());
//        echo '<br>';
//    }
//} catch (\Exception $e) {
//    echo get_class($e) . ' : ' . $e->getMessage();
//}


/* Devuelve la lista de SalesOrders filtradas por modified date */

try {
    $salesOrders = new \Picqer\Financials\Exact\SalesOrders($connection);

    $lastSyncDate = '2001-05-29 09:13:28'; //  -> Esto tiene que ser la fecha que se corrio la interfaz por ultima vez. Desde el log!!
    $lastSyncDate = date('Y-m-d\Th:i:s', strtotime($lastSyncDate));

    $lastSyncDate = "DateTime'$lastSyncDate'";

    $result = $salesOrders->filter('Modified gt ' . $lastSyncDate, '', '', 'Modified desc');
    foreach ($result as $salesOrder) {

        $salesOrdersArray = $salesOrder->attributes();


        //get the Account information for the order too
        $accounts = new \Picqer\Financials\Exact\Account($connection);

        $accountId = $salesOrdersArray['OrderedBy'];
        $resultAccount = $accounts->filter("ID eq guid'$accountId'");
        foreach ($resultAccount as $account) {
            $salesOrdersArray['Account'] = $account->attributes();
        }

        //get the Contant information for the order too

        $contactId = $salesOrdersArray['OrderedByContactPerson'];
        if ($contactId != '') {
            $contacts = new \Picqer\Financials\Exact\Contact($connection);
            $resultContact = $accounts->filter("ID eq guid'$accountId'");
            foreach ($resultContact as $contact) {
                $salesOrdersArray['Contact'] = $contact->attributes();
            }
        }


        //get the salesEntry (Products for each SO)
        $salesOrdersProducts = new \Picqer\Financials\Exact\SalesOrderLines($connection);
        $orderId = $salesOrdersArray['OrderID'];
        $filter = "OrderID eq guid'$orderId'";
        $resultProducts = $salesOrdersProducts->filter($filter);

        foreach ($resultProducts as $resultProduct) {
            $salesOrdersArray['Products'][] = $resultProduct->attributes();
        }


        var_dump($salesOrdersArray);
        echo '<br>';
    }
} catch (\Exception $e) {
    echo get_class($e) . ' : ' . $e->getMessage();
}



/* Devuelve la lista de Invoices filtradas por modified date*/

//try {
//    $salesOrders = new \Picqer\Financials\Exact\SalesInvoice($connection);
//    
//    $lastSyncDate = '2001-05-29 09:13:28'; //  -> Esto tiene que ser la fecha que se corrio la interfaz por ultima vez. Desde el log!!
//    $lastSyncDate = date('Y-m-d\Th:i:s', strtotime($lastSyncDate));
//    
//    $lastSyncDate = "DateTime'$lastSyncDate'" ;
//    
//    $result   = $salesOrders->filter('Modified gt ' .$lastSyncDate);
//    foreach ($result as $salesOrder) {
//        
//        $salesOrdersArray = $salesOrder->attributes();
//         
//         //get the salesEntry (Products for each SO)
//        $salesOrdersProducts = new \Picqer\Financials\Exact\SalesEntryLine($connection);
//        $orderId = $salesOrdersArray['InvoiceID'];
//        $filter = "InvoiceID eq guid'$orderId'";
//        $resultProducts   = $salesOrdersProducts->filter($filter);
//        
//        foreach ($resultProducts as $resultProduct) {
//            $salesOrdersArray['Products'][] = $resultProduct->attributes();
//        }
//        
//        
//         
//        var_dump($salesOrdersArray);
//        echo '<br>';
//    }
//} catch (\Exception $e) {
//    echo get_class($e) . ' : ' . $e->getMessage();
//}


/* Devuevle la lista completa the Currencies */

//try {
//    $journals = new \Picqer\Financials\Exact\Currencies($connection);
//    $result   = $journals->filter('','','Code,Description');
//    foreach ($result as $journal) {
//         var_dump($journal->attributes());
//        echo '<br>';
//    }
//} catch (\Exception $e) {
//    echo get_class($e) . ' : ' . $e->getMessage();
//}


/* Devuevle la lista completa the CostCenters */

//try {
//    $journals = new \Picqer\Financials\Exact\CostCenters($connection);
//    $result   = $journals->get();
//    foreach ($result as $journal) {
//         var_dump($journal->attributes());
//        echo '<br>';
//    }
//} catch (\Exception $e) {
//    echo get_class($e) . ' : ' . $e->getMessage();
//}


/* Devuevle la lista completa the Payments Conditions */

//try {
//    $journals = new \Picqer\Financials\Exact\PaymentCondition($connection);
//    $result   = $journals->get();
//    foreach ($result as $journal) {
//         var_dump($journal->attributes());
//        echo '<br>';
//    }
//} catch (\Exception $e) {
//    echo get_class($e) . ' : ' . $e->getMessage();
//}

/* Devuevle la lista completa the AccountClasifications */

//try {
//    $journals = new \Picqer\Financials\Exact\AccountClasifications($connection);
//    $result   = $journals->get();
//    foreach ($result as $journal) {
//         var_dump($journal->attributes());
//        echo '<br>';
//    }
//} catch (\Exception $e) {
//    echo get_class($e) . ' : ' . $e->getMessage();
//}


/* Devuevle la lista completa the Users */

//try {
//    $journals = new \Picqer\Financials\Exact\Users($connection);
//    $result   = $journals->get();
//    foreach ($result as $journal) {
//         var_dump($journal->attributes());
//        echo '<br>';
//    }
//} catch (\Exception $e) {
//    echo get_class($e) . ' : ' . $e->getMessage();
//}

/* Devuevle la lista completa the Warehouses */


//try {
//    $journals = new \Picqer\Financials\Exact\Warehouses($connection);
//    $result   = $journals->get();
//    foreach ($result as $journal) {
//         var_dump($journal->attributes());
//        echo '<br>';
//    }
//} catch (\Exception $e) {
//    echo get_class($e) . ' : ' . $e->getMessage();
//}

/* Devuevle la lista completa the VatCodes */


//try {
//    $journals = new \Picqer\Financials\Exact\VatCode($connection);
//    $result   = $journals->get();
//    foreach ($result as $journal) {
//         var_dump($journal->attributes());
//        echo '<br>';
//    }
//} catch (\Exception $e) {
//    echo get_class($e) . ' : ' . $e->getMessage();
//}


//try {
//    $journals = new \Picqer\Financials\Exact\Item($connection);
//    
//    $itemId = '054a3b16-f072-4da5-b367-86bd4382514c';
//    $result = $journals->filter("ID eq guid'$itemId'");
//    
//    //$result   = $journals->get();
//    foreach ($result as $journal) {
//         var_dump($journal->attributes());
//        echo '<br>';
//    }
//} catch (\Exception $e) {
//    echo get_class($e) . ' : ' . $e->getMessage();
//}


//try {
//    $accounts = new \Picqer\Financials\Exact\Account($connection);
//    
//    $accountId = 'bdca0ffd-0de7-400a-bf05-1ef01fe06fac';
//    $result = $accounts->filter("ID eq guid'$accountId'");
//    foreach ($result as $account) {
//         var_dump($account->attributes());
//        echo '<br>';
//    }
//} catch (\Exception $e) {
//    echo get_class($e) . ' : ' . $e->getMessage();
//}