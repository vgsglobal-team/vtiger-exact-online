<?php

/**
 * Todas las funciones que son especicas de la interfaz de Exact Online y
 * no es sirven para otros interfaces
 */

include_once 'modules/Vtiger/CRMEntity.php';
include_once 'include/utils/utils.php';
//include_once 'modules/Exactonline/utils/CommonUtils.php';
include_once 'modules/Exactonline/libs/ExactOnLineFunctions.php';

class ExactUtils {
    public function syncFieldsOptions() {
        $syncOptions[0]['value'] = 'get_from_eol';
        $syncOptions[0]['label'] = 'Get From EOL';
        $syncOptions[1]['value'] = 'push_to_eol';
        $syncOptions[1]['label'] = 'Push to EOL';
        $syncOptions[2]['value'] = 'bidirectional';
        $syncOptions[2]['label'] = 'Bi Directional';
        return $syncOptions;
    }

    public function getAccountType(){
        global $adb;
        $account_type = $adb->pquery("SELECT accounttype FROM vtiger_accounttype");
        while ($accounttype = $adb->fetch_array($account_type)) {
            $account_val[]['label'] = $accounttype['accounttype'];
        }
        return $account_val;
    }

    public function getConfiguredValues(){
        global $adb;
        $configurations = array();
        $configQuery = $adb->pquery("SELECT var,value FROM vtiger_exact_config where var not in ('clientid','clientsecret','division','code','access_token','refresh_token')", array());
        while ($config = $adb->fetch_array($configQuery)) {
            $configurations[$config['var']] = $config['value'];
        }
        return $configurations;
    }

    public function getAccountFields(){
        global $adb;
        $fields = array();
        $obj3 = $adb->pquery("SELECT fieldlabel, columnname, uitype FROM vtiger_field where (tablename = 'vtiger_account' || tablename='vtiger_accountscf')", array());
        while ($getField3 = $adb->fetch_array($obj3)) {
            $fields[] = $getField3;
        }
        return $fields;
    }

    public function getSalesOrderFields(){
        global $adb;
        $fields = array();
        $obj3 = $adb->pquery('SELECT fieldlabel, columnname, uitype FROM vtiger_field where (tablename = "vtiger_salesorder" || tablename="vtiger_salesordercf" || tablename="vtiger_crmentity") group by columnname', array());
        //$obj3 = $adb->pquery('SELECT fieldlabel, columnname, uitype FROM vtiger_field where (tablename = "vtiger_salesorder" || tablename="vtiger_salesordercf")', array());
        while ($getField3 = $adb->fetch_array($obj3)) {
            $fields[] = $getField3;
        }
        return $fields;
    }

    public function getProductFields(){
        global $adb;
        $fields = array();
        $query = $adb->pquery("SELECT tablename, fieldlabel, columnname, uitype, typeofdata, tabid FROM vtiger_field where (tablename = 'vtiger_productcf' || tablename = 'vtiger_products' || tablename = 'vtiger_crmentity') and typeofdata like '%V%' group by columnname", array());
        //$query = $adb->pquery("SELECT tablename, fieldlabel, columnname, uitype, typeofdata, tabid FROM vtiger_field where (tablename = 'vtiger_productcf' || tablename = 'vtiger_products') and typeofdata like '%V%'", array());
        while ($field = $adb->fetch_array($query)) {
            $fields[] = $field;
        }
        return $fields;
    }

    public function getVtigerUsers(){
        global $adb;
        $userQuery = $adb->pquery("SELECT user_name FROM vtiger_users", array());
        while ($user = $adb->fetch_array($userQuery)) {
            $users[] = $user;
        }
        return $users;
    }

    public function getInvoiceSendingMethod(){
        $i = 0;
        $sendingmethod = array();
        $methods = array(0 => '', 1 => 'Paper', 2 => 'E-mail', 4 => 'Digital postbox');
        foreach ($methods as $method_key => $single_method) {
            $sendingmethod[$i]['value'] = $method_key;
            $sendingmethod[$i]['label'] = $single_method;
            $i = $i + 1;
        }
        return $sendingmethod;
    }
    
    public function getSOStatus(){
        $i = 0;
        $soStatus = array();
        $status = array( 10 => 'Draft', 20 => 'Open');
        foreach ($status as $method_key => $single_method) {
            $soStatus[$i]['value'] = $method_key;
            $soStatus[$i]['label'] = $single_method;
            $i = $i + 1;
        }
        return $soStatus;
    }
    
    public function getVTSOStatus(){
        global $adb;
        $i = 0;
        $sostatus = array();
        $so = $adb->pquery("SELECT sostatus FROM vtiger_sostatus");
        while ($sostat = $adb->fetch_array($so)) {
            $sostatus[$i]['value'] = trim($sostat['sostatus']);
            $sostatus[$i]['label'] = trim($sostat['sostatus']);
            $i = $i + 1;
        }
        return $sostatus;
    }

    public function getVTProdCat(){
        global $adb;
        $i = 0;
        $prodcat = array();
        $pro = $adb->pquery("SELECT productcategory FROM vtiger_productcategory");
        while ($arr = $adb->fetch_array($pro)) {
            $prodcat[$i]['value'] = trim($arr['productcategory']);
            $prodcat[$i]['label'] = trim($arr['productcategory']);
            $i = $i + 1;
        }
        return $prodcat;
    }

    public function paralyseInstance(){
        $vatCodes = $this->getEOLVatCodes();
        foreach ($vatCodes as $vatCode) {
            $isPickListPresent = $this->isPickListPresent("vat_code", $vatCode['label']);
            updateConfig($vatCode['label'], "eol_vat_code_val_" . $vatCode['value']);
            if (!$isPickListPresent) {
                $this->addPick("vat_code", $vatCode['label']);
            }
        }

        $paymentConditions = $this->getEOLPaymentConditions();
        foreach ($paymentConditions as $paymentCondition) {
            $isPickListPresent = $this->isPickListPresent("payment_condition", $paymentCondition['label']);
            updateConfig($paymentCondition['label'], "eol_payment_condition_val_" . $paymentCondition['value']);
            if (!$isPickListPresent) {
                $this->addPick("payment_condition", $paymentCondition['label']);
            }
        }
    }

    public function isPickListPresent($pickListName, $label){
        global $adb;
        $pickListCheck = $adb->pquery("SELECT $pickListName FROM vtiger_$pickListName where $pickListName = ?", array($label));
        $pickListCount = $adb->num_rows($pickListCheck);
        if ($pickListCount > 0)
            return true;
        return false;
    }

    public function addPick($picklistname, $nametoinsert){
        global $adb, $log;
        $role_query = $adb->pquery("SELECT roleid FROM vtiger_role", array());
        while ($fetch_role = $adb->fetch_array($role_query)) {
            $roleid[] = $fetch_role;
        }
        $adb->pquery("UPDATE vtiger_picklistvalues_seq SET id=LAST_INSERT_ID(id+1)", array());
        $getPickListValue = $adb->query_result($adb->pquery("SELECT * FROM vtiger_picklistvalues_seq", array()), 0, 'id');
        $picklistid = $adb->query_result($adb->pquery("SELECT picklistid FROM vtiger_picklist WHERE name = ?", array($picklistname)), 0, "picklistid");
        $sortid = $adb->query_result($adb->pquery("SELECT max(distinct(sortid)) as max FROM vtiger_role2picklist WHERE picklistid = ?", array($picklistid)), 0, "max");

        if ($sortid == '' || empty($sortid) || !isset($sortid)) {
            $sortid = 1;
        }
        $pick_seq = $adb->query_result($adb->pquery("SELECT * FROM vtiger_{$picklistname}_seq", array()), 0, 'id');
        $adb->pquery("UPDATE vtiger_{$picklistname}_seq SET id=LAST_INSERT_ID(id+1)", array());
        $adb->pquery("INSERT INTO vtiger_{$picklistname} VALUES(?,?,1,?,?)", array($pick_seq, $nametoinsert, $getPickListValue, $sortid)); //VT6 columns changed
        foreach ($roleid as $role) {
            $adb->pquery("INSERT INTO vtiger_role2picklist VALUES(?, ?, ?, ?)", array($role['roleid'], $getPickListValue, $picklistid, $sortid));
        }
        return $getPickListValue;
    }

    public function getEOLUsers(){
        $EOLApi = new ExactOnlineApiWrapper();
        $connection = $EOLApi->connect();
        $users = array();
        $i = 0;
        try {
            $class = new \Picqer\Financials\Exact\Users($connection);
            $userss = $class->get();
            foreach ($userss as $user) {
                $user = $user->attributes();
                $users[$i]['id'] = $user['UserID'];
                $users[$i]['username'] = $user['UserName'];
                $users[$i]['label'] = $user['FullName'];
                $i++;
            }
        } catch (\Exception $e) {
            throw new Exception(get_class($e) . ' : ' . $e->getMessage());
        }
        return $users;
    }

    public function getEOLCostCenters(){
        $EOLApi = new ExactOnlineApiWrapper();
        $connection = $EOLApi->connect();
        $costCenters = array();
        
        try {
            $journals = new \Picqer\Financials\Exact\CostCenters($connection);
            $costCentersObj = $journals->get();
            foreach ($costCentersObj as $costCenterObj) {
                $costCenterObj = $costCenterObj->attributes();
                $costCenters[$i]['value'] = $costCenterObj['Code'];
                $costCenters[$i]['label'] = $costCenterObj['Description'];
                $i++;
            }
        } catch (\Exception $e) {
            echo get_class($e) . ' : ' . $e->getMessage();
        }
        return $costCenters;
    }
    
    public function getPaymentConditions(){
        $EOLApi = new ExactOnlineApiWrapper();
        $connection = $EOLApi->connect();
        $paymentConditions = array();
        
        try {
            $paymentCond = new \Picqer\Financials\Exact\PaymentCondition($connection);
            $paymentCondObjs = $paymentCond->get();
            foreach ($paymentCondObjs as $paymentCondObj) {
                $paymentCondObj = $paymentCondObj->attributes();
                $paymentConditions[$i]['value'] = $paymentCondObj['Code'];
                $paymentConditions[$i]['label'] = $paymentCondObj['Description'];
                $i++;
            }
        } catch (\Exception $e) {
            echo get_class($e) . ' : ' . $e->getMessage();
        }
        return $paymentConditions;
    }

    public function getEOLVatCodes(){
        $EOLApi = new ExactOnlineApiWrapper();
        $connection = $EOLApi->connect();
        $vats = array();
        
        try {
            $journals = new \Picqer\Financials\Exact\VatCode($connection);
            $vatCodeObj = $journals->get();
            foreach ($vatCodeObj as $vatObj) {
                $vatObj = $vatObj->attributes();
                $vats[$i]['value'] = $vatObj['Code'];
                $vats[$i]['label'] = $vatObj['Description'];
                $vats[$i]['percentage'] = $vatObj['Percentage'];
            }
        } catch (\Exception $e) {
            echo get_class($e) . ' : ' . $e->getMessage();
        }
        return $vats;
    }

    public function getEOLWarehouses(){
        $EOLApi = new ExactOnlineApiWrapper();
        $connection = $EOLApi->connect();
        $warehouse = array();
        $i = 0;
        try {
            $journals = new \Picqer\Financials\Exact\Warehouses($connection);
            $warehouseObj   = $journals->get();
            foreach ($warehouseObj as $eachWarehouse) {
                $eachWarehouse = $eachWarehouse->attributes();
                $warehouse[$i]['value'] = $eachWarehouse['Code'];
                $warehouse[$i]['label'] = $eachWarehouse['Description'];
                $i++;
            }
        } catch (\Exception $e) {
            echo get_class($e) . ' : ' . $e->getMessage();
        }
        return $warehouse;
    }

    public function getEOLAccountsClassification(){
        $EOLApi = new ExactOnlineApiWrapper();
        $connection = $EOLApi->connect();
        $classific = array();
        $i = 0;
        try {
            $journals = new \Picqer\Financials\Exact\AccountClasifications($connection);
            $classificationsObj   = $journals->get();
            foreach ($classificationsObj as $classification) {
                $classification = $classification->attributes();
                $classific[$i]['code'] = $classification['ID'];
                $classific[$i]['Description'] = $classification['Description'];
                $i++;
            }
        } catch (\Exception $e) {
            echo get_class($e) . ' : ' . $e->getMessage();
        }
        return $classific;
    }
    
    public function getEOLPaymentConditions(){
        $EOLApi = new ExactOnlineApiWrapper();
        $connection = $EOLApi->connect();
        $costCenters = array();
        $i = 0;
        try {
            $journals = new \Picqer\Financials\Exact\PaymentCondition($connection);
            $paymentConditionsObj = $journals->get();
            foreach ($paymentConditionsObj as $paymentConditionObj) {
                $paymentConditionObj = $paymentConditionObj->attributes();
                $costCenters[$i]['value'] = $paymentConditionObj['Code'];
                $costCenters[$i]['label'] = $paymentConditionObj['Description'];
                $i++;
            }
        } catch (\Exception $e) {
            echo get_class($e) . ' : ' . $e->getMessage();
        }
        return $costCenters;
    }

    function getCrmIdWithEOLCode($exactId, $module){
        global $adb;
        $crmIdQuery = $adb->pquery("select crmid from vtiger_exactonline_ids where exactid = ? and module = ? ", array($exactId, $module));
        return $adb->query_result($crmIdQuery, 0, 'crmid');
    }

    function getEOLCodeWithCrmId($crmId, $module){
        global $adb;
        $crmIdQuery = $adb->pquery("select exactid from vtiger_exactonline_ids where crmid = ? and module = ? ", array($crmId, $module));
        return $adb->query_result($crmIdQuery, 0, 'exactid');
    }
    
    public function getVTAssignedUser($EOLUserId){
        $db = PearDatabase::getInstance();
        $result = $db->pquery('SELECT * FROM vtiger_exact_config WHERE value=?', array($EOLUserId));
        
        $userId = '';
        
        if($result){
            if($db->num_rows($result) > 0 ){
                $userId = $db->query_result($result, 0, 'var');
                $userId = trim(str_replace('user_map_', '', $userId));
                $userModel = Users_Record_Model::getInstanceByName($userId);
                $userId = $userModel->getId();
                
            }
        }
        
        //no user id. Assign to the admin
        if($userId == ''){
            $userId = Users::getActiveAdminUser();
            $userId = $userId->id;
        }
        
        return $userId;
        
    }
    
    public function getInvoicingMethod() {
        $db = PearDatabase::getInstance();
        $result = $db->pquery("SELECT value FROM vtiger_exact_config WHERE var='account_invoice_method'");
        if($result && $db->num_rows($result)> 0){
            return $db->query_result($result, 0, 'value');
        }  else {
            return 1;
        }
    }
    
    
    public function getEOLAssignedUser($VTUserId){
        $db = PearDatabase::getInstance();
        
        $VTUserId = Users_Record_Model::getInstanceById($VTUserId, 'Users');
        $VTUserName = 'user_map_' . $VTUserId->get('user_name');
        
        $result = $db->pquery('SELECT * FROM vtiger_exact_config WHERE var=?', array($VTUserName));
        
        $userId = '';
        
        if($result){
            if($db->num_rows($result) > 0 ){
                $userId = $db->query_result($result, 0, 'value');
               
            }
        }
        
        return $userId;
        
    }
}