<?php

require_once 'modules/Exactonline/utils/ExactUtils.php';
require_once 'modules/Exactonline/utils/CommonUtils.php';
require_once 'include/Webservices/Create.php';
require_once 'include/Webservices/Revise.php';

class Exactonline_AccountManager {

    protected $lastSyncDate;

    function __construct() {
        $this->lastSyncDate = getLastSyncDate('Accounts');
        $this->lastSyncStatus = getLastRunningResult('Accounts');

        if ($this->lastSyncDate == false) {
            $this->lastSyncDate = 0;
        }

        try {
            $aux = new ExactOnlineApiWrapper();
            $this->eol_api = $aux->connect();
        } catch (Exception $exc) {
            //notifyErrors($exc);
            return false;
        }
    }

    function SyncAccounts() {
        global $pendingDownload;
        $pendingDownload = false;
        
        if($this->lastSyncStatus != 'pending' && $this->lastSyncDate > 0){
            $this->pushVTAccountsEOL();
        }
        
        $this->getEOLAccounts();
        
        if($pendingDownload){
            saveLog('Accounts', $pendingDownload, 'pending');
        }  else {
            saveLog('Accounts');
        }
    }

    function getEOLAccounts() {
        global $pendingDownload;
        
        
        try {
            $skip = '';
            $lastSyncDate = DateTimeField::convertToUserTimeZone(date('Y-m-d\Th:i:s',$this->lastSyncDate));
            $lastSyncDate = $lastSyncDate->format('Y-m-d\Th:i:sP');
            $lastSyncDate = "DateTime'$lastSyncDate'";

            $i = 1;
            $eolAccounts = array();
            while (true) {

                $accounts = new \Picqer\Financials\Exact\Account($this->eol_api);

                if ($skip != '') {
                    $result = $accounts->filter('Modified gt ' . $lastSyncDate, '', '', 'Modified asc', $skip, 50);
                } else {
                    $result = $accounts->filter('Modified gt ' . $lastSyncDate, '', '', 'Modified asc', 0, 50);
                }

                if (count($result) % 50 == 0 && count($result) != 0) {
                    $skip = intval($i * 50);
                }
                
                $eolAccounts = array_merge($eolAccounts, $result);

                if ($i == 2 && $skip != '') { // bajo hasta 100 cuentas maximo por vez
                    
                    $accountLast = end($eolAccounts);
                    $accountLastArray = $accountLast->attributes();
                    preg_match_all('!\d+!', $accountLastArray['Modified'], $matches);
                    $pendingDownload = intval($matches[0][0] / 1000);
                    break;
                }elseif ($skip == '') {
                    break;
                }
                $i++;
            }

            $updatedRecords = $this->updateCreateEOLAccounts($eolAccounts);
        } catch (\Exception $e) {
            echo get_class($e) . ' : ' . $e->getMessage();
        }

        return $updatedRecords;
    }

    function updateCreateEOLAccounts($eolAccounts) {
        $updatedRecords = array();
        
        foreach ($eolAccounts as $account) {
            $account = $account->attributes();
            $id = getCrmIdWithEOLCode($account['ID'], 'Accounts');
            if (!$id) {
                $id = $this->searchAccountByEmail($account['Email']);
                if ($id) {
                    $account['crmid'] = $id;
                    $this->createAccountFromEOL($account, 'Update');
                    insertSyncrIds($account['ID'], $id, 'Accounts');
                } else {
                    $this->createAccountFromEOL($account, 'Create');
                }
            } else {
                $account['crmid'] = $id;
                $this->createAccountFromEOL($account, 'Update');
            }

            if ($id) {
                $updatedRecords[] = $id;
            }
        }
        
        return $updatedRecords;
    }

    function pushVTAccountsEOL($updatedRecords) {
        global $pendingDownload;
        $account4Update = $this->getVTAccounts($updatedRecords);

        if ($account4Update) {
            foreach ($account4Update as $acc) {
                $account = new \Picqer\Financials\Exact\Account($this->eol_api);
                if ($acc['mode'] == 'Edit') {
                    $accid = $acc['ID'];
                    $account->filter("ID eq guid'$accid'");
                }

                foreach ($acc as $key => $value) {
                    $account->$key = $value;
                }

                $account->Status = C;
                $accountData = $account->save();
                $accountData = $accountData->attributes();

                insertSyncrIds($accountData['ID'], $acc['crmid'], 'Accounts');
            }
            
            $accountLast = end($account4Update);
            $pendingDownload = strtotime($accountLast['modifiedtime']);
            
        }
    }

    function getVTAccounts($updatedRecords) {
        $accounts = $this->getUnsyncVTAccounts();

        if ($this->lastSyncDate > 0) {
            $updatedAccounts = $this->getVTUpdatedAccounts($updatedRecords);
            $accounts = array_merge($accounts, $updatedAccounts);
        }

        if (count($accounts) > 0) {
            return $accounts;
        } else {
            return false;
        }
    }

    function getVTUpdatedAccounts($updatedRecords) {
        $db = PearDatabase::getInstance();
        $lastSync = date('Y-m-d h:i:s', intval($this->lastSyncDate));

        $sql = "SELECT * FROM vtiger_account acc 
                                    INNER JOIN vtiger_exactonline_ids eol ON acc.accountid = eol.crmid 
                                    INNER JOIN vtiger_accountbillads on acc.accountid = vtiger_accountbillads.accountaddressid
                                    INNER JOIN vtiger_accountshipads on acc.accountid = vtiger_accountshipads.accountaddressid
                                    INNER JOIN vtiger_accountscf on acc.accountid = vtiger_accountscf.accountid
                                    INNER JOIN vtiger_crmentity crm ON acc.accountid = crm.crmid 
                                    WHERE crm.deleted = 0";

        if (is_array($updatedRecords) && count($updatedRecords) > 0) {
            $sql .= ' AND acc.accountid NOT IN (' . implode(',', $updatedRecords) . ')';
        }

        $sql .= " AND crm.modifiedtime > ? AND crm.createdtime <> crm.modifiedtime ORDER BY modifiedtime ASC LIMIT 0, 50";

        $otroquery = $db->pquery($sql, array($lastSync));

        if ($db->num_rows($otroquery) > 0) {
            while ($arr = $db->fetch_array($otroquery)) {
                $accounts_aux['Name'] = $arr['accountname'];
                $accounts_aux['Email'] = $arr['email1'];
                $accounts_aux['Phone'] = $arr['phone'];
                $accounts_aux['AddressLine1'] = $arr['bill_street'];
                $accounts_aux['City'] = $arr['bill_city'];
                $accounts_aux['StateName'] = $arr['bill_state'];
                $accounts_aux['Postcode'] = $arr['bill_zip'];
                $accounts_aux['CountryName'] = $arr['bill_country'];
                $accounts_aux['crmid'] = $arr['accountid'];
                $accounts_aux['ID'] = $arr['exactid'];
                $accounts_aux['Website'] = $arr['website'];
                $accounts_aux['mode'] = 'Edit';
                $accounts_aux['modifiedtime'] = $arr['modifiedtime'];

                $EOLUser = ExactUtils::getEOLAssignedUser($arr['smownerid']);
                if ($EOLUser != '') {
                    $accounts_aux['Modifier'] = $EOLUser;
                }

                $accounts[] = $accounts_aux;
            }
        }

        return $accounts;
    }

    function getUnsyncVTAccounts() {
        $db = PearDatabase::getInstance();
        $query = $db->pquery("SELECT * FROM vtiger_account acc 
                                INNER JOIN vtiger_accountbillads on acc.accountid = vtiger_accountbillads.accountaddressid
                                INNER JOIN vtiger_accountshipads on acc.accountid = vtiger_accountshipads.accountaddressid
                                INNER JOIN vtiger_accountscf on acc.accountid = vtiger_accountscf.accountid
                                INNER JOIN vtiger_crmentity cr ON acc.accountid = cr.crmid 
                                WHERE cr.deleted = 0 AND acc.accountid NOT IN (SELECT crmid FROM vtiger_exactonline_ids WHERE module = 'Accounts') ORDER BY modifiedtime ASC LIMIT 0, 50", array());

        $accounts = array();

        if ($db->num_rows($query) > 0) {

            $invoicingMethod = ExactUtils::getInvoicingMethod();

            while ($arr = $db->fetch_array($query)) {
                $accounts_aux['Name'] = $arr['accountname'];
                $accounts_aux['Email'] = $arr['email1'];
                $accounts_aux['Phone'] = $arr['phone'];
                $accounts_aux['AddressLine1'] = $arr['bill_street'];
                $accounts_aux['City'] = $arr['bill_city'];
                $accounts_aux['StateName'] = $arr['bill_state'];
                $accounts_aux['Postcode'] = $arr['bill_code'];
                $accounts_aux['CountryName'] = $arr['bill_country'];
                $accounts_aux['crmid'] = $arr['accountid'];
                $accounts_aux['mode'] = 'Create';
                $accounts_aux['Website'] = $arr['website'];
                $accounts_aux['InvoicingMethod'] = $invoicingMethod;
                $accounts_aux['modifiedtime'] = $arr['modifiedtime'];

                $EOLUser = ExactUtils::getEOLAssignedUser($arr['smownerid']);
                if ($EOLUser != '') {
                    $accounts_aux['Creator'] = $EOLUser;
                }

                $accounts[] = $accounts_aux;
            }
        }

        return $accounts;
    }

    function createAccountFromEOL($accountData, $mode) {
        global $user;

        $orderId = $accountData['ID'];

        if ($user->id == '') {
            $user = getUserInstance();
        }

        $assignedUserId = ExactUtils::getVTAssignedUser($accountData['Modifier']);

        $accountInformation = array(
            'accountname' => $accountData['Name'],
            'email1' => $accountData['Email'],
            'phone' => $accountData['Phone'],
            'bill_street' => $accountData['AddressLine1'],
            'bill_city' => $accountData['City'],
            'bill_state' => $accountData['StateName'],
            'bill_code' => $accountData['Postcode'],
            'bill_country' => $accountData['CountryName'],
            'ship_street' => $accountData['AddressLine1'],
            'ship_city' => $accountData['City'],
            'ship_state' => $accountData['StateName'],
            'ship_code' => $accountData['Postcode'],
            'ship_country' => $accountData['CountryName'],
            'website' => $accountData['Website'],
            'assigned_user_id' => vtws_getWebserviceEntityId('Users', $assignedUserId)
        );

        try {
            if ($mode == 'Update') {
                $accountInformation['id'] = vtws_getWebserviceEntityId('Accounts', $accountData['crmid']);
                vtws_revise($accountInformation, $user);
            } else if ($mode == 'Create') {
                $accountCreated = vtws_create('Accounts', $accountInformation, $user);
                if ($accountCreated['id'] != '') {
                    insertSyncrIds($accountData['ID'], $accountCreated['id'], 'Accounts');
                }
            }
        } catch (Exception $exc) {
            notifyErrors($exc->message, $orderId);
            return false;
        }
        $accountCreated = explode('x', $accountCreated['id']);
        return $accountCreated[1];
    }

    function searchAccountByEmail($mail) {
        $db = PearDatabase::getInstance();
        $result = $db->pquery("SELECT accountid FROM vtiger_account acc INNER JOIN vtiger_crmentity crm ON acc.accountid = crm.crmid WHERE crm.deleted = 0 AND acc.email1 = ?", array($mail));
        if ($db->num_rows($result) > 0) {
            return $db->query_result($result, 0, 'accountid');
        } else {
            return false;
        }
    }

    function checkModified($accid, $eolDate) {
        $db = PearDatabase::getInstance();
        $result = $db->pquery("SELECT cr.createdtime,cr.modifiedtime,ac.accountname FROM `vtiger_account` ac inner join vtiger_crmentity cr on ac.accountid = cr.crmid where ac.accountid = ?", array($accid));

        $accountDate = $db->query_result($result, 0, 'modifiedtime');

        return ($eolDate > $accountDate);
    }

}
