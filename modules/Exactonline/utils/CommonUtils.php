<?php

include_once 'modules/Vtiger/CRMEntity.php';
include_once 'include/utils/utils.php';

function getCrmIdWithEOLCode($exactId, $module) {
    global $adb;
    $crmIdQuery = $adb->pquery("SELECT vtiger_exactonline_ids.crmid FROM vtiger_exactonline_ids INNER JOIN vtiger_crmentity ON vtiger_exactonline_ids.crmid = vtiger_crmentity.crmid  WHERE deleted =0 AND exactid = ? AND module = ? ", array($exactId, $module));
    if ($adb->num_rows($crmIdQuery) > 0) {
        return $adb->query_result($crmIdQuery, 0, 'crmid');
    } else {
        return false;
    }
}

function insertSyncrIds($eolRecordId, $crmId, $setype) {
    $db = PearDatabase::getInstance();
    $crmId = explode('x', $crmId);
    if (count($crmId) > 1) {
        $crmId = $crmId[1];
    } else {
        $crmId = $crmId[0];
    }
    $db->pquery("INSERT INTO vtiger_exactonline_ids(exactid, crmid, module) VALUES (?,?,?)", array($eolRecordId, $crmId, $setype));
}

function getLastSyncDate($mode) {
    $db = PearDatabase::getInstance();
    //$result = $db->pquery("SELECT running_time FROM vtiger_exactonline_log WHERE result != 'with errors' AND mode = ? ORDER BY logid DESC",array($mode));
    $result = $db->pquery("SELECT running_time FROM vtiger_exactonline_log WHERE mode = ? ORDER BY logid DESC", array($mode));
    if ($db->num_rows($result) > 0) {
        return $db->query_result($result, 0, 'running_time');
    } else {
        return false;
    }
}

if (!function_exists('getLastRunningResult')) {

    function getLastRunningResult($mode) {
        $db = PearDatabase::getInstance();
        $result = $db->query("SELECT result FROM vtiger_exactonline_log WHERE result != 'with errors' AND mode = '$mode'
                        ORDER BY logid DESC");
        if ($db->num_rows($result) > 0) {
            return $db->query_result($result, 0, 'result');
        } else {
            return false;
        }
    }

}

function saveLog($mode, $ultimafecha = '', $result = 'success') {
    global $errors;

    if ($errors) {
        $result = 'with errors';
    } else {
        $result = 'success';
    }

    if ($ultimafecha == '') {
        $ultimafecha = time();
    }

    $db = PearDatabase::getInstance();
    $db->pquery("INSERT INTO vtiger_exactonline_log (running_time,result,mode) VALUES (?,?,?)", array($ultimafecha, $result, $mode));
}

function getUserInstance() {
    $user = CRMEntity::getInstance('Users');
    $user->id = $user->getActiveAdminId();
    $user->retrieve_entity_info($user->id, 'Users');
    return $user;
}

/**
 * Function that returns the webservice Id of the user to whom the records are going
 * to be assigned to.
 * 
 * @global $settings array
 * @return Webservice id of the assigned user
 */
function getDefaultAsingUser() {
    global $settings;
    $ownerType = vtws_getOwnerType($settings['default_user_id']);
    return vtws_getWebserviceEntityId($ownerType, $settings['default_user_id']);
}

/**
 * Send an email to the assigned user id about the synching errors
 * 
 * @global type $settings
 * @global boolean $errors
 * @param type $msg
 * @param type $orderId
 */
function notifyErrors($msg, $orderId = '', $moduleName = '', $recordLabel = '') {
    require_once 'vtlib/Vtiger/Mailer.php';
    global $settings, $errors;

    $mailer = new Vtiger_Mailer();
    $mailer->IsHTML(true);
    if ($orderId == '' && $recordLabel != '') {
        $mailer->Subject = vtranslate('LBL_MAIL_ERROR_SUBJECT_2', 'VGSWooSynch') . vtranslate($moduleName) . ': ' . $recordLabel;
    } else {
        $mailer->Subject = vtranslate('LBL_MAIL_ERROR_SUBJECT', 'VGSWooSynch') . ' #' . $orderId;
    }

    $description = vtranslate('LBL_MAIL_ERROR_CONTENTS', 'VGSWooSynch');
    $description .= '<br><br>' . $msg;
    $description .= '<br><br>' . vtranslate('LBL_MAIL_ERROR_SALUTATION', 'VGSWooSynch');

    $mailer->Body = $description;

    //Notify the defualt assigned user
    $mailer->AddAddress(getUserEmail($settings['default_user_id']), getUserFullName($settings['default_user_id']));

    //CC the admin - Just in case
    $accountOwnerId = Users::getActiveAdminId();
    $mailer->AddCC(getUserEmail($accountOwnerId), getUserFullName($accountOwnerId));

    //Send the email
    $status = $mailer->Send(true);

    $errors = true;
}

function getCurrencyWsId($currencyCode) {
    global $user, $adb;

    if ($user->id == '') {
        $user = getUserInstance();
    }
    $queryResult = $adb->query("SELECT currencyid FROM vtiger_currencies WHERE currency_code like '$currencyCode'");
    /* /*$queryResult = vtws_query("SELECT id FROM Currency WHERE currency_code Like '$currencyCode';", $user);

      if (count($queryResult) > 0) {
      return $queryResult[0]['id'];
      } else {
      return false;
      } */
    if ($adb->num_rows($queryResult) > 0) {
        return $adb->query_result($queryResult, 0, 'currencyid');
    } else {
        return false;
    }
}

if (!function_exists(isModuleActivated)) {

    function isModuleActivated($moduleName) {
        $moduleInstance = Vtiger_Module_Model::getInstance($moduleName);
        if ($moduleInstance) {
            return $moduleInstance->isActive();
        } else {
            return false;
        }
    }

}

function getConfigValue($var) {
    $db = PearDatabase::getInstance();

    $value = Vtiger_Cache::get('vtiger-eol', $var);

    if ($value) {
        return $value;
    } else {
        $result = $db->pquery("SELECT value FROM vtiger_exact_config WHERE var = ?", array($var));

        if ($db->num_rows($result) > 0) {
            $value = $db->query_result($result, 0, 'value');
            Vtiger_Cache::set('vtiger-eol', $var, $value);
            return $value;
        } else {
            return false;
        }
    }
}

function updateConfig($var, $value) {
    $db = PearDatabase::getInstance();
    $configQuery = $db->pquery("SELECT value FROM vtiger_exact_config WHERE var = ?", array($var));
    $configValue = $db->query_result($configQuery, 0, 'value');
    if ($db->num_rows($configQuery) != 0)
        $db->pquery("UPDATE vtiger_exact_config set value = ? WHERE var = ?", array($value, $var));
    else
        $db->pquery("INSERT INTO vtiger_exact_config (var,value) VALUES (?,?)", array($var, $value));
}
