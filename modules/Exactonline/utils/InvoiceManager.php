<?php


require_once 'modules/VGSWooSynch/utils/class-wc-api-client.php';
require_once 'modules/VGSWooSynch/utils/Utils.php';
require_once 'modules/VGSWooSynch/utils/CustomerManager.php';
require_once 'modules/VGSWooSynch/utils/ProductsManager.php';

class VGSWooSynch_OrdersManager {

    protected $lastSyncDate;
    protected $target_inventory_module;
    protected $inventory_default_status;
    protected $inventory_tax_mode;
    protected $update_stock;
    protected $woo_order_status;
    protected $target_tax;

    function __construct() {
        global $wcApi, $errors;
        $errors = false;

        try {
            $wcApi = connectWoo();
            $this->wc_api = $wcApi;
        } catch (Exception $exc) {
            notifyErrors($exc);
            return false;
        }
    }

    private function vgsSetConstants() {
        $settings = woo_getSettings();


        $this->target_inventory_module = $settings['target_module'];
        $this->inventory_default_status = $settings['target_module_status'];
        $this->inventory_tax_mode = 'individual';
        if ($settings['update_stocks'] == 'on') {
            $this->update_stock = 1;
        } else {
            $this->update_stock = 0;
        }

        $this->woo_order_status = $settings['woo_order_status'];
        $this->target_tax = $settings['target_tax'];
    }

    /**
     * Function that download and creates the new orders from woocommerce site into vTiger
     */
    function SyncOrders($orderId = '') {

        $this->vgsSetConstants();
        $this->lastSyncDate = getLastSyncDate();

        if ($orderId != '') {
            $allNewOrders = $this->getWooOrderById($orderId);
        } else {
            $allNewOrders = $this->getAllWooOrders();
        }

        if (!is_array($allNewOrders)) {
            $allNewOrders = array($allNewOrders);
        }



        if (count($allNewOrders) > 0) {
            $allNewOrders = $this->getNewRecords($allNewOrders);
        }

        if (count($allNewOrders) > 0) {
            foreach ($allNewOrders as $newOrder) {
                $this->addOrder($newOrder);
            }
        }

        if ($orderId == '') {
            saveLog();
        }
    }

    function updateWooOrder($orderId, $status) {

        $orderArray['order']['id'] = $orderId;
        $orderArray['order']['status'] = $status;

        $allProducts = $this->wc_api->update_in_woo('orders', $orderId, $orderArray, 'PUT');
    }

    function getWooOrderById($orderId) {
        $order = $this->wc_api->get_order($orderId);

        if (!is_array($order)) {
            return array($order);
        } else {
            return $order;
        }
    }

    /**
     * Download all the orders created after the last synch date
     * 
     * @param type $modifiedDate
     * @return array
     */
    function getAllWooOrders() {
        if (!$this->lastSyncDate) {
            $modifiedDate = 0;
        } else {
            $modifiedDate = date('Y-m-d G:h:s', $this->lastSyncDate);
        }

        $allNewOrders = array();

        $i = 1;
        while (true) {

            $orders = $this->wc_api->get_orders($params = array('status' => $this->woo_order_status, 'filter[updated_at_min]' => $modifiedDate, 'page' => $i, 'filter[limit]' => 20));

            if (count($orders->orders) == 0) {
                break;
            }
            $allNewOrders = array_merge($allNewOrders, $orders->orders);
            $i++;
        }


        return $allNewOrders;
    }

    /**
     * Return an array of records not in vtiger
     * 
     * @param type $allRecords Array of all woocommerce orders
     * @return type
     */
    function getNewRecords($allRecords) {

        foreach ($allRecords as $key => $wooRecord) {
            if (recordExist($wooRecord->id, $this->target_inventory_module)) {
                unset($allRecords[$key]);
            }
        }

        return $allRecords;
    }

    /**
     * Creates a new Invoice in vTiger using Woocommerce Information
     * 
     * @global type $user
     * @param type $newOrder
     * @return boolean
     */
    function addOrder($newOrder) {
        global $user;

        if ($user->id == '') {
            $user = getUserInstance();
        }

        $customerManager = new VGSWooSynch_CustomerManager();
        $customerCRMId = $customerManager->getParentId($newOrder);

        if (!$customerCRMId) {
            return false;
        }

        $currencyWsId = getCurrencyWsId($newOrder->currency);
        if (!$currencyWsId) {
            notifyErrors(vtranslate('LBL_CURRENCY_NOT_AVAILABLE', 'VGSWooSynch'));
            return false;
        }

        $lineItems = $this->buildItemsArray($newOrder);
        if (!$lineItems) {
            return false;
        }

        try {


            $data = array(
                'subject' => 'WooCommerce Order #' . $newOrder->id,
                'account_id' => $customerCRMId,
                'productid' => $lineItems[0]['productid'],
                'hdnS_H_Amount' => $newOrder->shipping_lines[0]->total,
                'bill_street' => $newOrder->billing_address->address_1,
                'bill_city' => $newOrder->billing_address->city,
                'bill_state' => $newOrder->billing_address->state,
                'bill_code' => $newOrder->billing_address->postcode,
                'bill_country' => $newOrder->billing_address->country,
                'ship_street' => $newOrder->shipping_address->address_1,
                'ship_city' => $newOrder->shipping_address->city,
                'ship_state' => $newOrder->shipping_address->state,
                'ship_code' => $newOrder->shipping_address->postcode,
                'ship_country' => $newOrder->shipping_address->country,
                'currency_id' => $currencyWsId,
                'hdnTaxType' => $this->inventory_tax_mode,
                'assigned_user_id' => getDefaultAsingUser(),
                'LineItems' => $lineItems,
            );

            if ($newOrder->billing_address->address_1 == '') {

                $data['bill_street'] = $newOrder->shipping_address->address_1;
                $data['bill_city'] = $newOrder->shipping_address->city;
                $data['bill_state'] = $newOrder->shipping_address->state;
                $data['bill_code'] = $newOrder->shipping_address->postcode;
                $data['bill_country'] = $newOrder->shipping_address->country;
            }

            switch ($this->target_inventory_module) {
                case 'SalesOrder':
                    $data['sostatus'] = $this->inventory_default_status;
                    $data['invoicestatus'] = 'AutoCreated';

                    break;
                case 'Quotes':
                    $data['quotestage'] = $this->inventory_default_status;

                    break;

                default:
                    $data['invoicestatus'] = $this->inventory_default_status;
                    break;
            }




            $Invoice = vtws_create($this->target_inventory_module, $data, $user);
            if (is_array($Invoice) && array_key_exists('id', $Invoice) && $Invoice['id'] != '') {

                insertSynchIds($newOrder->id, $Invoice['id'], $this->target_inventory_module);

                if ($this->update_stock == 1) {
                    $productManager = New VGSWooSynch_ProductManager();
                    $productManager->updateProductsStocks($lineItems, $Invoice['id']);
                }

                $this->addNoteToOrder($newOrder->id, $Invoice['id']);
            }
        } catch (WebServiceException $exc) {
            notifyErrors($exc->message, $newOrder->id);
            return false;
        }
    }

    /**
     * Builds a product array form woocommerce order
     * 
     * @param type $newOrder
     * @return boolean|array
     */
    function buildItemsArray($newOrder) {
        $adb = PearDatabase::getInstance();
        $productsManager = new VGSWooSynch_ProductManager();

        $lineItems = array();
        foreach ($newOrder->line_items as $item) {
            $productId = recordExist($item->product_id, 'Products');
            if (!$productId) {

                $productId = $productsManager->createUpdateVTProduct($item, $newOrder->id);
            }

            if (!$productId) {
                return false;
            }


            $lineItem = array(
                'productid' => $productId,
                'listprice' => $item->price,
                'quantity' => $item->quantity,
            );

            $taxes = $this->setTaxes($item, $productId);

            $lineItem = array_merge($lineItem, $taxes);

            array_push($lineItems, $lineItem);
        }

        return $lineItems;
    }

    function setTaxes($item, $productId) {
        $taxes = array();
        $adb = PearDatabase::getInstance();

        if ($this->inventory_tax_mode == 'individual') {
            $productsManager = new VGSWooSynch_ProductManager();
            $taxes = $productsManager->getVTProductTaxes($productId);

            $tax_per = round(round(($item->total_tax / $item->total), 3) * 100, 2);

            if (!isset($taxes[$this->target_tax])) {

                $res = $adb->pquery("SELECT taxid FROM vtiger_inventorytaxinfo WHERE taxname=?", array($this->target_tax));
                if ($res && $adb->num_rows($res) > 0) {
                    $taxid = $adb->query_result($res, 0, 'taxid');
                    $productCrmId = explode('x', $productId);
                    $query = "INSERT INTO vtiger_producttaxrel VALUES(?,?,?)";
                    $adb->pquery($query, array($productCrmId[1], $taxid, $tax_per));
                }
            }
            $taxes[$this->target_tax] = $tax_per;
        }
        return $taxes;
    }

    function addNoteToOrder($wooOrderId, $vtRecordId) {
        global $wcApi, $errors;

        if ($this->wc_api == '') {
            try {
                $wcApi = connectWoo();
                $this->wc_api = $wcApi;
            } catch (Exception $exc) {
                notifyErrors($exc);
                return false;
            }
        }
        
        $vtRecordId = explode('x', $vtRecordId);
        
        $params['order_note']['note'] = 'Order created in CRM. Record Id: ' . $vtRecordId[1];
              
      
        $this->wc_api->make_custom_endpoint_call('orders/' . $wooOrderId . '/notes',$params,'POST');
        
    }
    
   

}
