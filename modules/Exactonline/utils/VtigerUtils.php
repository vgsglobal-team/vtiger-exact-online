<?php

/**
 * Funciones que tienen que ver con Vtiger
 */


include_once 'modules/Vtiger/CRMEntity.php';
include_once 'include/utils/utils.php';
require_once 'include/Webservices/DescribeObject.php';
require_once 'include/Webservices/Utils.php';
require_once 'include/Webservices/Query.php';
require_once 'modules/VGSWooSynch/utils/class-wc-api-client.php';

/**
 * Function that returns and array fo settings
 * 
 * @global type $settings
 * @return type
 */

function woo_getSettings() {
    global $settings;

    $db = PearDatabase::getInstance();
    $result = $db->query("SELECT * FROM vtiger_vgswoosynch");
    while ($result_set = $db->fetch_array($result)) {
        $settings = $result_set;
    }

    return $settings;
}

/**
 * Connects to Woocommerce Site
 * 
 * @return \WC_API_Client
 */
function connectWoo() {
    global $wcApi;

    $settings = woo_getSettings();
    $wcApi = new WC_API_Client($settings['woo_key'], $settings['woo_secret'], $settings['site_url']);

    return $wcApi;
}

/**
 * Given a woocommerce id return the webservice id of the object
 * 
 * @param type $wooRecordId
 * @param type $setype
 * @return boolean false is record do not exist in the CRM. The webservice id otherwise
 */

if (!function_exists(recordExist)) {

    function recordExist($wooRecordId, $setype) {

        $db = PearDatabase::getInstance();
        $result = $db->pquery("SELECT woocommerceid,vtiger_vgswoosynch_ids.crmid FROM vtiger_vgswoosynch_ids 
                            INNER JOIN vtiger_crmentity ON vtiger_vgswoosynch_ids.crmid = vtiger_crmentity.crmid
                            WHERE deleted=0
                            AND woocommerceid=? AND vtiger_vgswoosynch_ids.setype=?", array($wooRecordId, $setype));
        if ($db->num_rows($result) > 0) {
            return vtws_getWebserviceEntityId($setype, $db->query_result($result, 0, 'crmid'));
        } else {
            return false;
        }
    }

}

if (!function_exists(searchByLabel)) {

    function searchByLabel($wooRecordLabel, $wooRecordId, $setype) {

        $db = PearDatabase::getInstance();
        $result = $db->pquery("SELECT crmid FROM vtiger_crmentity WHERE deleted=0 AND setype=? AND label=?", array($setype, $wooRecordLabel));
        if ($db->num_rows($result) > 0) {
            return $db->query_result($result, 0, 'crmid');
        } else {
            return false;
        }
    }

}

/**
 * Given a VT id return the Woocommerce id of the object
 * 
 * @param type vTiger crm id
 * @param type $setype
 * @return boolean false is record do not exist in the CRM. The webservice id otherwise
 */
function getWoocommerceId($crmRecordId, $setype) {

    $db = PearDatabase::getInstance();
    $result = $db->pquery("SELECT woocommerceid,vtiger_vgswoosynch_ids.crmid FROM vtiger_vgswoosynch_ids 
                            WHERE vtiger_vgswoosynch_ids.crmid=? AND vtiger_vgswoosynch_ids.setype=?", array($crmRecordId, $setype));
    if ($db->num_rows($result) > 0) {
        return $db->query_result($result, 0, 'woocommerceid');
    } else {
        return false;
    }
}

/**
 * Given a VT id return the Woocommerce id of the object
 * 
 * @param type vTiger crm id
 * @param type $setype
 * @return boolean false is record do not exist in the CRM. The webservice id otherwise
 */
function getVTcrmId($wooId, $setype) {

    $db = PearDatabase::getInstance();
    $result = $db->pquery("SELECT vtiger_vgswoosynch_ids.crmid FROM vtiger_vgswoosynch_ids 
                            INNER JOIN vtiger_crmentity ON vtiger_vgswoosynch_ids.crmid = vtiger_crmentity.crmid
                            WHERE deleted=0 AND vtiger_vgswoosynch_ids.woocommerceid=? AND vtiger_vgswoosynch_ids.setype=?", array($wooId, $setype));
    if ($db->num_rows($result) > 0) {
        return $db->query_result($result, 0, 'crmid');
    } else {
        return false;
    }
}


/**
 * Given a vTiger crmid return the woocommerce id of the object
 * 
 * @param type $vtRecordId
 * @param type $setype
 * @return boolean false is record do not exist in the CRM. The webservice id otherwise
 */
function VTrecordExist($vtRecordId, $setype) {

    $db = PearDatabase::getInstance();
    $result = $db->pquery("SELECT woocommerceid,vtiger_vgswoosynch_ids.crmid FROM vtiger_vgswoosynch_ids 
                            INNER JOIN vtiger_crmentity ON vtiger_vgswoosynch_ids.crmid = vtiger_crmentity.crmid
                            WHERE deleted=0
                            AND vtiger_vgswoosynch_ids.crmid=? AND vtiger_vgswoosynch_ids.setype=?", array($vtRecordId, $setype));
    if ($db->num_rows($result) > 0) {
        return true;
    } else {
        return false;
    }
}



/**
 * Function that store in database the equivalence between a woocommerce id and vtiger crm id
 * 
 * @param type $wooRecordId
 * @param type $crmId
 * @param type $setype
 */
function insertSynchIds($wooRecordId, $crmId, $setype) {
    $db = PearDatabase::getInstance();
    $crmId = explode('x', $crmId);
    $crmId = $crmId[1];
    $result = $db->pquery("INSERT INTO vtiger_vgswoosynch_ids (crmid, woocommerceid, setype) VALUES (?,?,?)", array($crmId, $wooRecordId, $setype));
}

/**
 * Initiate a new user instance
 * 
 * @return type user instance
 */
function getUserInstance() {
    $user = CRMEntity::getInstance('Users');
    $user->id = $user->getActiveAdminId();
    $user->retrieve_entity_info($user->id, 'Users');
    return $user;
}

/**
 * Function that returns the webservice Id of the user to whom the records are going
 * to be assigned to.
 * 
 * @global $settings array
 * @return Webservice id of the assigned user
 */
function getDefaultAsingUser() {
    global $settings;
    $ownerType = vtws_getOwnerType($settings['default_user_id']);
    return vtws_getWebserviceEntityId($ownerType, $settings['default_user_id']);
}

/**
 * Returns the orders currency webservice id 
 * 
 * @param type $currencyCode
 * @return Currency webservice id of false
 */
function getCurrencyWsId($currencyCode) {
    global $user;

    if ($user->id == '') {
        $user = getUserInstance();
    }

    $queryResult = vtws_query("SELECT id FROM Currency WHERE currency_code Like '$currencyCode';", $user);

    if (count($queryResult) > 0) {
        return $queryResult[0]['id'];
    } else {
        return false;
    }
}

/**
 * Send an email to the assigned user id about the synching errors
 * 
 * @global type $settings
 * @global boolean $errors
 * @param type $msg
 * @param type $orderId
 */
function notifyErrors($msg, $orderId = '', $moduleName = '', $recordLabel = '') {
    require_once 'vtlib/Vtiger/Mailer.php';
    global $settings, $errors;

    $mailer = new Vtiger_Mailer();
    $mailer->IsHTML(true);
    if($orderId == '' && $recordLabel != ''){
        $mailer->Subject = vtranslate('LBL_MAIL_ERROR_SUBJECT_2', 'VGSWooSynch') . vtranslate($moduleName) . ': ' . $recordLabel;
    }  else {
        $mailer->Subject = vtranslate('LBL_MAIL_ERROR_SUBJECT', 'VGSWooSynch') . ' #' . $orderId;
    }
    
    $description = vtranslate('LBL_MAIL_ERROR_CONTENTS', 'VGSWooSynch');
    $description .= '<br><br>' . $msg;
    $description .= '<br><br>' . vtranslate('LBL_MAIL_ERROR_SALUTATION', 'VGSWooSynch');
    
    $mailer->Body = $description;

    //Notify the defualt assigned user
    $mailer->AddAddress(getUserEmail($settings['default_user_id']), getUserFullName($settings['default_user_id']));

    //CC the admin - Just in case
    $accountOwnerId = Users::getActiveAdminId();
    $mailer->AddCC(getUserEmail($accountOwnerId), getUserFullName($accountOwnerId));

    //Send the email
    $status = $mailer->Send(true);

    $errors = true;
}

if(!function_exists(isModuleActivated)){
    function isModuleActivated($moduleName){
        $moduleInstance = Vtiger_Module_Model::getInstance($moduleName);
        if($moduleInstance){
            return $moduleInstance->isActive();
        }  else {
            return false;
        }
    }
}
