<?php
                
require_once 'modules/Exactonline/utils/ExactUtils.php';
require_once 'modules/Exactonline/utils/CommonUtils.php';
require_once 'include/Webservices/Create.php';
require_once 'include/Webservices/Revise.php';

class Exactonline_SalesOrderManager {

    protected $lastSyncDate;

    function __construct() {
        $this->lastSyncDate = getLastSyncDate('SalesOrder');

        if ($this->lastSyncDate == false) {
            $this->lastSyncDate = 0;
        }

        try {
            $aux = new ExactOnlineApiWrapper();
            $this->eol_api = $aux->connect();
        } catch (\Exception $exc) {
            echo get_class($exc) . ' : ' . $exc->getMessage();
            //notifyErrors($exc);
            return false;
        }
    }

    function SyncSalesOrder() {
        $this->getEOLSalesOrder();
        $this->createSalesOrderEOL();
    }
    
    function createSalesOrderEOL(){
        $salesorder4Update = $this->findSO2Update();
        
        if($salesorder4Update){
            foreach ($salesorder4Update as $so) {
                $salesOrder = new \Picqer\Financials\Exact\SalesOrders($this->eol_api);
                if($so['mode'] == 'Edit'){
                    $soid = $so['OrderID'];
                    $salesOrder->filter("OrderID eq guid'$soid'");
                }
                
                foreach ($so as $key => $value) {
                    $salesOrder->$key = $value;
                }
                
                $salesOrderData = $salesOrder->save();                
                $salesOrderData = $salesOrderData->attributes();
                
                insertSyncrIds($salesOrderData['OrderID'], $so['crmid'], 'SalesOrder');
                saveLog('SalesOrder');
            }
        }    
    }
    
    function findSO2Update(){
        $db = PearDatabase::getInstance();
        $query = $db->pquery("SELECT * FROM vtiger_salesorder so INNER JOIN vtiger_crmentity cr ON so.salesorderid = cr.crmid WHERE cr.deleted = 0 AND so.salesorderid NOT IN (SELECT crmid FROM vtiger_exactonline_ids WHERE module = 'SalesOrder')", array());
        
        $salesOrders = array();
        
        //@TODO: - Falta el sync the paymemts conditions
        //@TODO: - Falta respetar el mapping de los campos
        //@TODO: - Falta equivalencia entre Status
        
        
        
        if ($db->num_rows($query) > 0) {            
            while($arr = $db->fetch_array($query)){
                $items = $this->getSOProducts($arr['salesorderid']);
                $salesOrder['crmid'] = $arr['salesorderid'];
                $salesOrder['OrderID'] = ExactUtils::getEOLCodeWithCrmId($arr['salesorderid'],'SalesOrder');
                $salesOrder['OrderedBy'] = ExactUtils::getEOLCodeWithCrmId($arr['accountid'],'Accounts');
                $salesOrder['InvoiceTo'] = ExactUtils::getEOLCodeWithCrmId($arr['accountid'],'Accounts');
                $salesOrder['YourRef'] = $arr['subject'];
                $salesOrder['PaymentCondition'] = '1';
                $salesOrder['LineItems'] = $items;
                $salesOrder['mode'] = 'Create';
                
                $EOLUser = ExactUtils::getEOLAssignedUser($arr['smownerid']);
                if($EOLUser != ''){
                    $salesOrder['Creator'] = $EOLUser;
                }
                
                $salesOrders[] = $salesOrder;
            }
        }
        $lastSync = date('Y-m-d h:i:s',intval($this->lastSyncDate));
        $lastSync = date('Y-m-d H:i:s', strtotime($lastSync . '-5 minutes'));

        $otroquery = $db->pquery("SELECT * FROM vtiger_salesorder so INNER JOIN vtiger_exactonline_ids eol ON so.salesorderid = eol.crmid INNER JOIN vtiger_crmentity crm ON so.salesorderid = crm.crmid WHERE crm.deleted = 0 AND crm.modifiedtime > ? AND crm.createdtime <> crm.modifiedtime", array($lastSync));
        if ($db->num_rows($otroquery) > 0) {
            while($arr = $db->fetch_array($otroquery)){
                $items = $this->getSOProducts($arr['salesorderid']);
                $salesOrder['crmid'] = $arr['salesorderid'];
                $salesOrder['OrderID'] = ExactUtils::getEOLCodeWithCrmId($arr['salesorderid'],'SalesOrder');
                $salesOrder['OrderedBy'] = ExactUtils::getEOLCodeWithCrmId($arr['accountid'],'Accounts');
                $salesOrder['InvoiceTo'] = ExactUtils::getEOLCodeWithCrmId($arr['accountid'],'Accounts');
                $salesOrder['YourRef'] = $arr['subject'];
                $salesOrder['PaymentCondition'] = '1';
                $salesOrder['LineItems'] = $items;
                $salesOrder['mode'] = 'Edit';
                
                $EOLUser = ExactUtils::getEOLAssignedUser($arr['smownerid']);
                if($EOLUser != ''){
                    $salesOrder['Modifier'] = $EOLUser;
                }
                
                $salesOrders[] = $salesOrder;
            }
        }
        if(count($salesOrders) > 0){
            return $salesOrders;
        }else{
            return false;
        }
    }
    
    function getSOProducts($soid){
        $db = PearDatabase::getInstance();
        $query = $db->pquery("SELECT * FROM vtiger_inventoryproductrel WHERE id = ?",array($soid));

        $items = array();
        
        if ($db->num_rows($query) > 0) {            
            while($arr = $db->fetch_array($query)){
                $items[] = [
                    'Item' => ExactUtils::getEOLCodeWithCrmId($arr['productid'],'Products'),
                    'Quantity' => $arr['quantity'],
                    'UnitPrice' => $arr['listprice'],
                    'Discount' => floatval($arr['discount_percent']) / 100,
                ]; 
            }
        }
        if(count($items) > 0){
            return $items;
        }else{
            return false;
        }      
    }
    
    function getEOLSalesOrder(){
        try {
            $milisegundos = 0; 
            $skip = '';
            $lastSyncDate = $this->lastSyncDate;
            $lastSyncDate = date('Y-m-d\Th:i:s', $lastSyncDate);
            $lastSyncDate = "DateTime'$lastSyncDate'";
            
            $i = 1;
            while (true) {
                   
                $salesOrders = new \Picqer\Financials\Exact\SalesOrders($this->eol_api);
             
                if ($skip != '') {
                    $result = $salesOrders->filter('Modified gt ' . $lastSyncDate, '', '', 'Modified asc', $skip, 50);
                } else {
                    $result = $salesOrders->filter('Modified gt ' . $lastSyncDate, '', '', 'Modified asc', 0, 50);
                }

                if (count($result) == 0) {
                    saveLog('SalesOrder');
                    break;
                }
                foreach ($result as $salesOrder) {
                    $salesOrdersArray = $salesOrder->attributes();
                    $salesOrdersProducts = new \Picqer\Financials\Exact\SalesOrderLines($this->eol_api);//get the salesOrder (Products for each SO)
                    $orderId = $salesOrdersArray['OrderID'];
                    $resultProducts = $salesOrdersProducts->filter("OrderID eq guid'$orderId'");
                    foreach ($resultProducts as $resultProduct) {
                        $salesOrdersArray['Products'][] = $resultProduct->attributes();
                    }
                    $id = getCrmIdWithEOLCode($orderId, 'SalesOrder');
                    if(!$id){
                        $idfromlabel = searchByLabel($salesOrdersArray['Description'], 'SalesOrder');
                        if($idfromlabel){
                            $this->createSalesOrder($salesOrdersArray,'Update');
                            insertSyncrIds($salesOrdersArray['OrderID'], $idfromlabel, 'SalesOrder');
                        }else{
                            $this->createSalesOrder($salesOrdersArray,'Create');
                        }
                    }else{
                        $salesOrdersArray['crmid'] = $id;
                        $this->createSalesOrder($salesOrdersArray,'Update');
                    }                
                }
                
                if(count($result) % 50 == 0){
                    $skip = intval($i * 50);
                }
                
                if($i == 5 && $skip != ''){ // bajo hasta 250 cuentas maximo por vez
                    preg_match_all('!\d+!', $salesOrdersArray['Modified'], $matches);
                    $milisegundos = intval($matches[0][0]) / 1000;
                    saveLog('SalesOrder', $milisegundos);
                    break;
                } elseif ($skip == '') {
                    saveLog('SalesOrder');
                    break;
                }else{
                    $i++;
                }       
            } 
        } catch (\Exception $e) {
            echo get_class($e) . ' : ' . $e->getMessage() . '<br>';
            var_dump($salesOrdersArray);
        }
    }

    function createSalesOrder($soData, $mode) {
        require_once 'modules/Exactonline/utils/AccountManager.php';
        $EOLAccountManager = new Exactonline_AccountManager();
        global $user;

        if ($user->id == '') {
            $user = getUserInstance();
        }
        
        $currencyId = getCurrencyWsId($soData['Currency']);
        if (!$currencyId) {
            echo vtranslate('LBL_CURRENCY_NOT_AVAILABLE', 'VGSWooSynch');
            return false;
        }
        
        $lineItems = $this->buildItemsArray($soData);
        if (!$lineItems) {
            return false;
        }
        
        $accId = getCrmIdWithEOLCode($soData['InvoiceTo'], 'Accounts');
        try {
            $accounts = new \Picqer\Financials\Exact\Account($this->eol_api);
            $accountId = $soData['InvoiceTo'];
            $result = $accounts->filter("ID eq guid'$accountId'");
            if(count($result)>0){
                foreach ($result as $account) {
                    $account = $account->attributes();
                    if(!$accId){
                        $accId = $EOLAccountManager->createAccountFromEOL($account,'Create');
                    }else{
                        preg_match_all('!\d+!', $account['Modified'], $matches);
                        $miliseg = intval($matches[0][0]) / 1000;
                        $date = date("Y-m-d H:i:s",$miliseg);
                        if($EOLAccountManager->checkModified($accId,$date)){
                            $account['crmid'] = $accId;
                            $EOLAccountManager->createAccountFromEOL($account,'Update');
                        }
                    }
                }
            }else{
                return false;
            }
        } catch (\Exception $e) {
            echo get_class($e) . ' : ' . $e->getMessage();
        }

        $calculatedTotal = $this->getTotal($soData['Products']);
        
        $accountRecord = Vtiger_Record_Model::getInstanceById($accId, 'Accounts');
        
        $subject = ($soData['YourRef'] === '') ? 'ExactOnline Order #' . $soData['OrderNumber'] : $soData['YourRef'];
        
        $data = array(
            'subject' => $subject,
            'account_id' => vtws_getWebserviceEntityId('Accounts',$accId),
            'productid' => vtws_getWebserviceEntityId('Products',$lineItems[0]['productid']),
            //'hdnS_H_Amount' => $calculatedTotal,
            'total'=> $calculatedTotal,
            'bill_street' => $accountRecord->get('bill_street'),
            'bill_city' => $accountRecord->get('bill_city'),
            'bill_state' => $accountRecord->get('bill_state'),
            'bill_code' => $accountRecord->get('bill_code'),
            'bill_country' => $accountRecord->get('bill_country'),
            'ship_street' => $accountRecord->get('ship_street'),
            'ship_city' => $accountRecord->get('ship_city'),
            'ship_state' => $accountRecord->get('ship_state'),
            'ship_code' => $accountRecord->get('ship_code'),
            'ship_country' => $accountRecord->get('ship_country'),
            'currency_id' => vtws_getWebserviceEntityId('Currency',$currencyId),
            'assigned_user_id' => vtws_getWebserviceEntityId('Users', $user->id),
            'LineItems' => $lineItems,
            'sostatus' => "Created",
            'invoicestatus' => "Created",
        );

        if ($mode == 'Update'){
            $data['id'] = vtws_getWebserviceEntityId('SalesOrder', $soData['crmid']);
            vtws_revise($data, $user);
        }else if ($mode == 'Create'){
            $Invoice = vtws_create('SalesOrder', $data, $user);

            if ($Invoice['id'] != '') {
                insertSyncrIds($soData['OrderID'], $Invoice['id'], 'SalesOrder');
            }
        }
    }
    
    function getTotal($arrayProductos){
        $total = 0;
        foreach ($arrayProductos as $product) {
            $total += $product['AmountDC'];
        }
        return $total;
    }
    
    function buildItemsArray($newOrder) {
        $lineItems = array();
        foreach ($newOrder['Products'] as $product) {
            $itemId = $product['Item'];
            $productId = getCrmIdWithEOLCode($itemId, 'Products');
            if(!$productId){
                try {
                    $products = new \Picqer\Financials\Exact\Account($this->eol_api);
                    $result = $products->filter("ID eq guid'$itemId'");
                    if(count($result)>0){
                        foreach ($result as $product) {
                            $product = $product->attributes();
                            require_once 'modules/Exactonline/utils/ProductsManager.php';
                            $EOLProductManager = new Exactonline_ProductManager();
                            $productId = $EOLProductManager->createProduct($product,'Create');
                        }
                    }else{
                        return false;
                    }
                } catch (\Exception $e) {
                    echo get_class($e) . ' : ' . $e->getMessage();
                }
            }

            $lineItem = array(
                'productid' => vtws_getWebserviceEntityId('Products',$productId),
                'listprice' => $product['UnitPrice'],
                'quantity' => $product['Quantity'],
                'discount_percent' => floatval($product['Discount']) * 100,
            );

            //$taxes = $this->setTaxes($item, $productId);
            //$lineItem = array_merge($lineItem, $taxes);

            array_push($lineItems, $lineItem);
        }

        return $lineItems;
    }
}