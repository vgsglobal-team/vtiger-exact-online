<?php

require_once 'modules/Exactonline/utils/ExactUtils.php';
require_once 'modules/Exactonline/utils/CommonUtils.php';
require_once 'include/Webservices/Create.php';
require_once 'include/Webservices/Revise.php';

class Exactonline_ProductManager {

    protected $lastSyncDate;

    function __construct() {
        $this->lastSyncDate = getLastSyncDate('Products');

        if ($this->lastSyncDate == false) {
            $this->lastSyncDate = 0;
        }

        try {
            $aux = new ExactOnlineApiWrapper();
            $this->eol_api = $aux->connect();
        } catch (Exception $exc) {
            //notifyErrors($exc);
            return false;
        }
    }

    function SyncProducts() {
        global $lastDateProd;
        $lastDateProd = false;

        $this->getProductsFromEOL();
        if (!$lastDateProd) {
            $this->createProductsEOL();
        }

        if ($lastDateProd) {
            saveLog('Products', $lastDateProd);
        }
    }

    function getProductsFromEOL() {
        global $lastDateProd;

        try {
            $milisegundos = 0;
            $skip = '';
            $lastSyncDate = $this->lastSyncDate;
            $lastSyncDate = date('Y-m-d\Th:i:s', $lastSyncDate);
            $lastSyncDate = "DateTime'$lastSyncDate'";

            $EOLProducts = array();

            $i = 1;
            while (true) {

                $products = new \Picqer\Financials\Exact\Item($this->eol_api);

                if ($skip != '') {
                    $EOLDownProducts = $products->filter('Modified gt ' . $lastSyncDate, '', '', 'Modified asc', $skip, 50);
                } else {
                    $EOLDownProducts = $products->filter('Modified gt ' . $lastSyncDate, '', '', 'Modified asc', 0, 50);
                }

                $EOLProducts = array_merge($EOLProducts, $EOLDownProducts);

                if (count($EOLProducts) % 50 == 0) {
                    $skip = $i * 50;
                } elseif (count($EOLProducts) == 0) {
                    break;
                }


                if (($i == 2 && $skip != '') || $skip == '') {

                    $productLast = end($EOLProducts);
                    $productLastArray = $productLast->attributes();
                    preg_match_all('!\d+!', $productLastArray['Modified'], $matches);
                    $lastDateProd = intval($matches[0][0] / 1000);

                    break;
                } else {
                    $i++;
                }
            }

            $this->createProdVT($EOLProducts);
        } catch (\Exception $e) {
            echo get_class($e) . ' : ' . $e->getMessage();
        }
    }

    function createProdVT($EOLProducts) {

        if (!is_array($EOLProducts))
            return;

        foreach ($EOLProducts as $product) {
            $product = $product->attributes();
            $id = getCrmIdWithEOLCode($product['ID'], 'Products');
            if (!$id) {
                $id = searchByLabel($product['Description'], 'Products');
                if ($id) {
                    $product['crmid'] = $id;
                    $this->createProduct($product, 'Update');
                    insertSyncrIds($product['ID'], $id, 'Products');
                } else {
                    $this->createProduct($product, 'Create');
                }
            } else {
                $product['crmid'] = $id;
                $this->createProduct($product, 'Update');
                insertSyncrIds($product['ID'], $id, 'Products');
            }
        }
    }

    function createProductsEOL() {
        $prods4Update = $this->findProds2Update();

        if ($prods4Update) {
            foreach ($prods4Update as $product) {
                $item = new \Picqer\Financials\Exact\Item($this->eol_api);
                $item->Code = $product['Code'];
                $item->CostPriceStandard = $product['CostPriceStandard'];
                $item->Description = $product['Description'];
                $item->ExtraDescription = $product['ExtraDescription'];
                $item->IsSalesItem = true;
                $item->SalesVatCode = '03';
                $itemData = $item->save();
                $itemData = $itemData->attributes();

                insertSyncrIds($itemData['ID'], $product['crmid'], 'Products');
            }
        }
    }

    function findProds2Update() {
        global $lastDateProd;

        $db = PearDatabase::getInstance();
        $query = $db->pquery("SELECT pr.*,description, currency_code 
                                FROM vtiger_products pr 
                                INNER JOIN vtiger_crmentity cr ON pr.productid = cr.crmid 
                                LEFT JOIN vtiger_productcurrencyrel ON pr.productid = vtiger_productcurrencyrel.productid
                                LEFT JOIN vtiger_currency_info ON vtiger_productcurrencyrel.currencyid = vtiger_currency_info.`id`
                                WHERE cr.deleted = 0 
                                AND pr.productid NOT IN (SELECT crmid FROM vtiger_exactonline_ids WHERE module = 'Products') 
                                ORDER BY modifiedtime ASC LIMIT 0, 50", array());

        $products = array();

        //@TODO: - Falta el currency y respetar el mapping de los campos
        //@TODO: - Equivalencia entre categorias VT <-> EOL
        //@TODO: - Equivalencia entre unidades de Medidas VT <-> EOL


        if ($db->num_rows($query) > 0) {
            while ($arr = $db->fetch_array($query)) {
                $products_aux['Description'] = $arr['productname'];
                $products_aux['CostPriceStandard'] = $arr['unit_price'];
                $products_aux['ExtraDescription'] = $arr['description'];
                $products_aux['Code'] = $arr['productcode'];
                $products_aux['crmid'] = $arr['productid'];
                $products_aux['mode'] = 'Create';

                $EOLUser = ExactUtils::getEOLAssignedUser($arr['smownerid']);
                if ($EOLUser != '') {
                    $products_aux['Creator'] = $EOLUser;
                }

                $products[] = $products_aux;
            }
        }




        $lastSync = date('Y-m-d h:i:s', intval($this->lastSyncDate / 1000));

        $otroquery = $db->pquery("SELECT * FROM vtiger_products pr 
                                        INNER JOIN vtiger_exactonline_ids eol ON pr.productid = eol.crmid 
                                        INNER JOIN vtiger_crmentity crm ON pr.productid = crm.crmid 
                                        LEFT JOIN vtiger_productcurrencyrel ON pr.productid = vtiger_productcurrencyrel.productid
                                        LEFT JOIN vtiger_currency_info ON vtiger_productcurrencyrel.currencyid = vtiger_currency_info.`id`
                                        WHERE crm.deleted = 0 AND crm.modifiedtime > ? 
                                        AND crm.createdtime <> crm.modifiedtime
                                        ORDER BY modifiedtime ASC
                                        LIMIT 0, 50", array($lastSync));

        if ($db->num_rows($otroquery) > 0) {
            while ($arr = $db->fetch_array($otroquery)) {
                $products_aux['Description'] = $arr['productname'];
                $products_aux['CostPriceStandard'] = $arr['unit_price'];
                $products_aux['ExtraDescription'] = $arr['description'];
                $products_aux['Code'] = $arr['productcode'];
                $products_aux['crmid'] = $arr['productid'];
                $products_aux['ID'] = $arr['exactid'];
                $products_aux['mode'] = 'Edit';

                $EOLUser = ExactUtils::getEOLAssignedUser($arr['smownerid']);
                if ($EOLUser != '') {
                    $products_aux['Creator'] = $EOLUser;
                }

                $products[] = $products_aux;
            }

            $productLast = end($products);
            $lastDateProd = strtotime($productLast['modifiedtime']);
        }

        if (count($products) > 0) {
            return $products;
        } else {
            return false;
        }
    }

    function createProduct($productData, $mode) {
        global $user;

        if ($user->id == '') {
            $user = getUserInstance();
        }

        $assignedUserId = ExactUtils::getVTAssignedUser($productData['Modifier']);

        $productInformation = array(
            'productname' => $productData['Description'],
            'assigned_user_id' => vtws_getWebserviceEntityId('Users', $assignedUserId),
            'unit_price' => $productData['CostPriceStandard'],
            'description' => $productData['ExtraDescription'],
            'productcode' => $productData['Code'],
            'discontinued' => 1, // activo = true
        );

        if ($mode == 'Update') {
            $productInformation['id'] = vtws_getWebserviceEntityId('Products', $productData['crmid']);
            vtws_revise($productInformation, $user);
        } else if ($mode == 'Create') {
            $productInformation = vtws_create('Products', $productInformation, $user);
            $prodId = explode('x', $productInformation['id'])[1];

            if ($prodId != '') {
                insertSyncrIds($productData['ID'], $prodId, 'Products');
            }
        }
        return $productInformation['id'];
    }

}
