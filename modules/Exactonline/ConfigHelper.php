<?php

class ConfigHelper{

	/* Updates the config table */
        function updateConfig($var,$value){
                global $adb;
                $configQuery = $adb->pquery("select value from vtiger_exact_config where var = ?",array($var));
                $configValue = $adb->query_result($configQuery,0,'value');
                if($adb->num_rows($configQuery) != 0)
                        $adb->pquery("update vtiger_exact_config set value = ? where var = ?",array($value,$var));
                else
                        $adb->pquery("insert into vtiger_exact_config (var,value) values (?,?)",array($var,$value));
		return true;
        }
} 
