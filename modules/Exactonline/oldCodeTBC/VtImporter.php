<?php
require 'modules/Accounts/Accounts.php';
require 'modules/SalesOrder/SalesOrder.php';
require 'modules/Invoice/Invoice.php';

class VtImporter{

	private $focus, $assigned_user_id;
	private $currentmodule;
	public $updated = 1;
	public $eolId, $crmId;

	public function __construct($module) {
		global $current_user;
		$current_user->id = 1;
		$this->currentmodule = $module;
		$this->focus = new $module();
		//TODO: set current user id
		$this->assigned_user_id = 1;
	}

	public function setEolId($eolId){
		$this->eolId = $eolId;
	}

	public function setAllColumnFields($columnFields) {
		$this->isRecordExist();
		foreach ($columnFields as $key => $columnField) {
			$this->focus->column_fields[$key] = $columnField;
		}
		$this->focus->column_fields['assigned_user_id'] = $this->assigned_user_id;
	}	

	public function setInventoryRelations($lineItems){
		$productIds = $lineItems['product_ids'];
		$qTy = $lineItems['quantity'];
		$totalPrice = $lineItems['total_price'];
		$tAx = $lineItems['vat_amount'];
		$discAmt = $lineItems['discount'];

		global $adb;
		$adb->pquery("delete from vtiger_inventoryproductrel where id = ?",array($this->focus->id));
		$pro_query = "insert into vtiger_inventoryproductrel (id, productid, sequence_no, quantity, listprice,tax1,discount_amount) values (?,?,?,?,?,?,?)";
		$subTot = $tottax = $disc_Amt = 0;
		$tot = $tottax1 = 0;
		for ($i = 0; $i < count($productIds); $i++) {
			$check_pro = $adb->pquery($pro_query, array($this->focus->id, $productIds[$i], $i, $qTy[$i], $totalPrice[$i], $tAx[$i], $discAmt[$i]));
			$qtywithprice = $qTy[$i] * $totalPrice[$i];
			$subTot += $qtywithprice + ($qtywithprice * ($tAx[$i] / 100));
			$disc_Amt += $discAmt[$i];
		}
		$tot = $subTot + $shippingamount - $disc_Amt;
		$type = strtolower($this->currentmodule);
		$type1 = $type;
		if ($type == 'quotes') {
			$type1 = 'quote';
		}

		$adb->pquery("update vtiger_$type set total = '{$tot}',conversion_rate = '{$conversion_rate}',subtotal = '{$subTot}', s_h_amount = '{$shippingamount}',taxtype='individual' where " . $type1 . "id = {$this->focus->id}", array());

	}

	public function isRecordExist() {
		global $adb;
		$focusId_query = $adb->pquery("select crmid from vtiger_exactonline_ids where exactid = ? and module = ?", array($this->eolId, $this->currentmodule));
		$focusId = $adb->query_result($focusId_query, 0, 'crmid');
		if ($focusId) {
			$focusId = $this->IsCrmidExist($focusId, $this->currentmodule);
		}
		if ($focusId) {
			$this->crmId = $focusId;
			$this->focus->mode = "edit";
			$this->focus->id = $focusId;
			$this->focus->retrieve_entity_info($focusId, $this->currentmodule);
			$this->updated = 2;
		}
	}

        /* If record deleted in vtiger, then add a new record */
        public function IsCrmidExist($crmid, $module) {
                global $adb;
                $checkCrmIdExists_query = $adb->pquery("select * from vtiger_crmentity where crmid = ? and deleted = 0", array($crmid));
                $checkCrmidExist = $adb->query_result($checkCrmIdExists_query, 0, 'crmid');
                if ($checkCrmidExist) {
                        return $checkCrmidExist;
                } else {
                        $adb->pquery("delete from vtiger_exactonline_ids where crmid = ? and module = ?", array($crmid, $module));
                        return false;
                }
        }

	public function save() {
		$this->focus->assigned_user_id = $this->assigned_user_id;
		$this->focus->save($this->currentmodule);
		$this->crmId = $this->focus->id;
		$this->maintainIntegration();
	}

	public function maintainIntegration(){
                global $adb;
error_log("Updated - ".$this->updated." eol id - ".$this->eolId." crmid - ".$this->focus->id,3,"/tmp/eol.log");
                if ($this->updated != 2) {
                        $adb->pquery('insert into vtiger_exactonline_ids (exactid,crmid,module,source) values (?,?,?,?)', array($this->eolId, $this->focus->id, $this->currentmodule,"ExactOnline"));
                }
	}
}
