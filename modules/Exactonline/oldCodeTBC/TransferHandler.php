<?php
class TransferHandler{

	public $idsTransferred = array();
	public $vtigerModule = null;

        function __construct($eventId, $syncType){
                $this->eventId = $eventId;
                $this->syncType = $syncType;
		$this->vtigerModule = $this->getVtigerModule();
                $this->initiateProcess();
        }

	public function initiateProcess(){
		$this->collectData();
	}

	public function sendInvoice($crmId){
		global $adb;
		require "modules/Exactonline/invoiceSyncMapper.php";
		$invoiceFocus = new Invoice();
		$invoiceFocus->id = $crmId;
		$invoiceFocus->retrieve_entity_info($crmId,"Invoice");
                $productsQuery = $adb->pquery("select * from vtiger_inventoryproductrel where id = ? ", array($crmId));
		$noofrows = $adb->num_rows($productsQuery);
                $magproducts = array();
                for ($in1 = 0; $in1 < $noofrows; $in1++) {
                        $tax1 = $tax2 = $tax3 = null;
                        $taxamount = 0;
                        $crmproductIds = $adb->query_result($productsQuery, $in1, 'productid');
                        $getMagProdId = getEOLCodeWithCrmId($crmproductIds,"Products");
                        $magproducts[$in1]['id'] = $getMagProdId;
                        $magproducts[$in1]['qty'] = $adb->query_result($productsQuery, $in1, 'quantity');
                        $magproducts[$in1]['listprice'] = $adb->query_result($productsQuery, $in1, 'listprice');
                        $magproducts[$in1]['discount_percent'] = $adb->query_result($productsQuery, $in1, 'discount_percent');
                        $magproducts[$in1]['discount_amount'] = $adb->query_result($productsQuery, $in1, 'discount_amount');
                        if ($magproducts[$in1]['discount_percent']) {
                                $magproducts[$in1]['discount_amount'] = $magproducts[$in1]['listprice'] * $magproducts[$in1]['qty'] * ($magproducts[$in1]['discount_percent'] / 100);
                        }
                        $tax1 = $adb->query_result($productsQuery, $in1, 'tax1');
                        $tax2 = $adb->query_result($productsQuery, $in1, 'tax2');
                        $tax3 = $adb->query_result($productsQuery, $in1, 'tax3');
                        if ($tax1) {
                                $taxamount += (($magproducts[$in1]['listprice'] * $magproducts[$in1]['qty']) - $magproducts[$in1]['discount_amount']) * ($tax1 / 100);
                        }
                        if ($tax2) {
                                $taxamount += (($magproducts[$in1]['listprice'] * $magproducts[$in1]['qty']) - $magproducts[$in1]['discount_amount']) * ($tax2 / 100);
                        }
                        if ($tax3) {
                                $taxamount += (($magproducts[$in1]['listprice'] * $magproducts[$in1]['qty']) - $magproducts[$in1]['discount_amount']) * ($tax3 / 100);
                        }
                        $magproducts[$in1]['tax_amount'] = $taxamount;
                }


	}


	public function sendSalesOrder($crmId){
		global $adb;
		require "modules/Exactonline/salesOrderSyncMapper.php";
		$salesOrderFocus = new SalesOrder();
		$salesOrderFocus->id = $crmId;
		$salesOrderFocus->retrieve_entity_info($crmId,"SalesOrder");
		$productsQuery = $adb->pquery("select * from vtiger_inventoryproductrel where id = ? ", array($crmId));

		$noofrows = $adb->num_rows($productsQuery);
		$magproducts = array();
		for ($in1 = 0; $in1 < $noofrows; $in1++) {
			$tax1 = $tax2 = $tax3 = null;
			$taxamount = 0;
			$crmproductIds = $adb->query_result($productsQuery, $in1, 'productid');
			$getMagProdId = getEOLCodeWithCrmId($crmproductIds,"Products");
			$magproducts[$in1]['id'] = $getMagProdId;
			$magproducts[$in1]['qty'] = $adb->query_result($productsQuery, $in1, 'quantity');
			$magproducts[$in1]['listprice'] = $adb->query_result($productsQuery, $in1, 'listprice');
			$magproducts[$in1]['discount_percent'] = $adb->query_result($productsQuery, $in1, 'discount_percent');
			$magproducts[$in1]['discount_amount'] = $adb->query_result($productsQuery, $in1, 'discount_amount');
			if ($magproducts[$in1]['discount_percent']) {
				$magproducts[$in1]['discount_amount'] = $magproducts[$in1]['listprice'] * $magproducts[$in1]['qty'] * ($magproducts[$in1]['discount_percent'] / 100);
			}
			$tax1 = $adb->query_result($productsQuery, $in1, 'tax1');
			$tax2 = $adb->query_result($productsQuery, $in1, 'tax2');
			$tax3 = $adb->query_result($productsQuery, $in1, 'tax3');
			if ($tax1) {
				$taxamount += (($magproducts[$in1]['listprice'] * $magproducts[$in1]['qty']) - $magproducts[$in1]['discount_amount']) * ($tax1 / 100);
			}
			if ($tax2) {
				$taxamount += (($magproducts[$in1]['listprice'] * $magproducts[$in1]['qty']) - $magproducts[$in1]['discount_amount']) * ($tax2 / 100);
			}
			if ($tax3) {
				$taxamount += (($magproducts[$in1]['listprice'] * $magproducts[$in1]['qty']) - $magproducts[$in1]['discount_amount']) * ($tax3 / 100);
			}
			$magproducts[$in1]['tax_amount'] = $taxamount;
		}



	}

	public function sendItems($crmId){
		require 'modules/Exactonline/itemSyncMapper.php';
		$productFocus = new Products();
		$productFocus->id = $crmId;
		$productFocus->retrieve_entity_info($crmId,'Products');
		$eolData = array();
		foreach($mappedFields as $eolField => $vtigerField){
			$eolField = str_replace("d:","",$eolField);
			$eolData[$eolField] = $productFocus->column_fields[$vtigerField];
		}
		$eolCode = getEOLCodeWithCrmId($crmId,"Products");
                if($eolCode){
                        $eolData['ID'] = $eolCode;
                }
		$eolApi = new EOL_API("Items");
		$eolApi->refreshAccessToken();
		$url = $eolApi->getUrl();
		$response = $eolApi->postWithCurl($url,$eolData);
		if($eolApi->exceptionRaised){
			$message = $eolApi->message;
			$response = $eolApi->response;
			$this->updateLog($eolApi->exceptionRaised,$message,$response,$crmId);
		}
	}

	public function sendAccount($crmId){
		require 'modules/Exactonline/accountSyncMapper.php';
		$accountFocus = new Accounts();
		$accountFocus->id = $crmId;
		$accountFocus->retrieve_entity_info($crmId, 'Accounts');
		$eolData = array();
		foreach($mappedFields as $eolField => $vtigerField){
			$eolField = str_replace("d:","",$eolField);
			$eolData[$eolField] = $accountFocus->column_fields[$vtigerField];	
		}
		$eolCode = getEOLCodeWithCrmId($crmId,"Accounts");
		if($eolCode){
			$eolData['ID'] = $eolCode;

		}
		$eolApi = new EOL_API($this->vtigerModule);
		$accessToken = $eolApi->refreshAccessToken();
		if($accessToken){
			$eolData['access_token'] = $accessToken;
			$accountUrl = $eolApi->getUrl();
			$response = $eolApi->postWithCurl($accountUrl,$eolData);
			if($eolApi->exceptionRaised){
				$message = $eolApi->message;
				$response = $eolApi->response;
				$this->updateLog($eolApi->exceptionRaised,$message,$response,$crmId);
			}
		}
	}

	public function updateLog($exceptionRaised,$message,$response,$crmId){

		global $adb;
		$adb->pquery("insert into vtiger_exact_synceventlog(synceventid,created_time,exception,message,response,crmid) values (?,?,?,?,?,?)",array($this->eventId,date("Y-m-d h:i:s"),$exceptionRaised,$message,$response,$crmId));
	}

	public function transferData($crmId,$modifiedTime){
		if($this->vtigerModule == "Accounts")
			$this->sendAccount($crmId);
		elseif($this->vtigerModule == "Products"){
			$this->sendItems($crmId);
		}
		elseif($this->vtigerModule == "SalesOrder"){
			$this->sendSalesOrder($crmId);
		}
		elseif($this->vtigerModule == "Invoice"){
			$this->sendInvoice($crmId);
		}
		else{
		}
		$this->updateModifiedTime($modifiedTime);
	}

	public function updateModifiedTime($modifiedTime){
		global $adb;
		$updateModifiedTime = $adb->pquery("update vtiger_exact_syncevent set recordsmodifiedtime = ? where synceventid = ?",array($modifiedTime,$this->eventId));
	}

	public function collectData($crmId = NULL, $modifiedTime = NULL){
		global $adb;
		if($modifiedTime == NULL){
			$recModQuery = $adb->pquery("select recordsmodifiedtime from vtiger_exact_syncevent where synceventid = ?",array($this->eventId));
			$recMod = $adb->query_result($recModQuery,0,'recordsmodifiedtime');
			if($recMod == NULL)
				$crmIdQuery = 'select crmid,modifiedtime from vtiger_crmentity where setype = "'.$this->vtigerModule.'" and deleted != 1 order by modifiedtime ASC,crmid ASC limit 1';
			else
				$crmIdQuery = 'select crmid,modifiedtime from vtiger_crmentity where setype = "'.$this->vtigerModule.'" and deleted != 1 and modifiedtime >="'. $recMod.'" order by modifiedtime ASC,crmid ASC limit 1';
		}else{
			$this->idsTransferred[] = $crmId;
			$crmIdQuery = 'select crmid,modifiedtime from vtiger_crmentity where setype = "'.$this->vtigerModule.'" and deleted != 1 and modifiedtime >="'. $modifiedTime.'" and crmid not in ('.implode(',',$this->idsTransferred).') order by modifiedtime ASC,crmid ASC limit 1';
		}

		$crmIdPQuery = $adb->pquery($crmIdQuery);
		$crmId = $adb->query_result($crmIdPQuery,0,'crmid');
		$modifiedTime = $adb->query_result($crmIdPQuery,0,'modifiedtime');
		if($crmId){
			$this->transferData($crmId,$modifiedTime);	
			$this->collectData($crmId,$modifiedTime);
		}else{
		}
	}

        public function getVtigerModule(){
                if($this->syncType == "send_accounts")
                        return "Accounts";
                if($this->syncType == "send_products")
                        return "Products";
                if($this->syncType == "send_salesorder")
                        return "SalesOrder";
                if($this->syncType == "send_invoice")
                        return "Invoice";
		if($this->syncType == "send_contacts")
			return "Contacts";
        }
}
