<?php

class DownloadHandler{

	public $module,$syncType;
	const INITIAL = "Initialised";
	const DOWNLOADED = "Downloaded";

	/* Initiates downlaod data */

	function __construct($eventId, $syncType){
		$this->syncType = $syncType;
		$this->module = $this->getModuleForSyncType();
		$this->initiateDownloadData($eventId);
	}

	/* collects the download params for each */
	function collectDownloadParams(){
		$selectFields = $this->getSelectFields();
		if($this->module != "Items")
			$params['$orderby'] = "Modified asc";
                if($selectFields != false)
                        $params['$select'] = implode(",",$selectFields);
		if($this->module == "SalesOrders")
			$params['$expand'] = 'SalesOrderLines';
		if($this->module == "Invoices")
			$params['$expand'] = "SalesInvoiceLines";
		return $params;
	}

	/* request data to EOL API, writes the response */
	function initiateDownloadData($eventId,$downloadUrl = false){

		$downloadId = $this->initiateDownload($eventId);
		$filePath = "modules/Exactonline/temp/".$this->module."_".$eventId."_".$downloadId.".xml";

		$eolApi = new EOL_API($this->module);

		if(!$downloadUrl){
			$params = $this->collectDownloadParams();
			$response = $eolApi->get($params);
		}
		else
			$response = $eolApi->get(array(),$downloadUrl);

		$this->writeResponse($response,$eventId,$filePath);
		$p = xml_parser_create();
		xml_parse_into_struct($p, $response, $vals, $index);
		xml_parser_free($p);

		$nextDownloadLink = false;
		$link = $vals[$index['LINK'][(count($index['LINK']) -1)]];
		if($link['level'] == 2){
			$nextDownloadLink = $link['attributes']['HREF'];
		}

		$this->updateDownloadDetails($downloadId,$eventId,$filePath,self::DOWNLOADED,$nextDownloadLink,0);
		if($nextDownloadLink){
			//free memory to solve memory issue
			unset($response);unset($vals);unset($index);
			$this->initiateDownloadData($eventId,$nextDownloadLink);
		}

		$this->updateDownloadCompleteState($eventId);
	}

	/* updates the status of the download for the event */

	function updateDownloadCompleteState($eventId){
		global $adb;
		$adb->pquery("update vtiger_exact_syncevent set state = ?, updated_time = ? where synceventid = ?",array(self::DOWNLOADED,date("Y-m-d h:i:s"),$eventId));
		$adb->pquery("update vtiger_exact_download set state = ? where synceventid = ?",array(self::DOWNLOADED,$eventId));

	}

	/* initiate the download, by inserting the new entry */

	public function initiateDownload($eventId){
		global $adb;
		$adb->pquery("insert into vtiger_exact_download (synceventid,state) values(?,?)",array($eventId,self::INITIAL));
		return $adb->getLastInsertID();
	}

	/* updates the download data */

	public function updateDownloadDetails($downloadId,$eventId,$filePath,$state,$nextDownloadLink,$processingIndex){
		global $adb;
		$adb->pquery("update vtiger_exact_download set xmlurl = ?, state = ?, nextdownloadlink = ?, processingindex = ? where downloadid = ?",array($filePath,$state,$nextDownloadLink,$processingIndex,$downloadId));
		return true;
	} 

	/* returns last downloaded id  of the processing event*/

	public function getDownloadId($eventId){
		global $adb;
		$downloadIdQuery = $adb->pquery("select downloadid from vtiger_exact_download where synceventid = ? order by downloadid desc",array($eventId));
		return $adb->query_result($downloadIdQuery,0,'downloadid');
	}

	/* below method writes the response  in the given path*/

	public function writeResponse($response,$eventId,$filePath){
		$fp = fopen($filePath, 'w');
		fwrite($fp, $response);
		fclose($fp);
		return $filePath;
	}

	/* Returns the module name with sync type TODO: Change to switch case */

	public function getModuleForSyncType(){
		if($this->syncType == "get_accounts")
			return "Accounts";
		if($this->syncType == "get_products")
			return "Items";
		if($this->syncType == "get_salesorder")
			return "SalesOrders";
		if($this->syncType == "get_invoice")
			return "Invoices";
	}

	/* returns the selected fields */
	public function getSelectFields(){
		if($this->syncType == "get_accounts"){
			global $accountModuleFields;
			return $accountModuleFields;
		}
		if($this->syncType == "get_products"){
			global $productModuleFields;
			return $productModuleFields;
		}
		if($this->syncType == "get_salesorder"){
			global $salesOrderModuleFields;
			return $salesOrderModuleFields;
		}

		return false;
	}
	
}
