<?php
global $adb;
$getCrmIdForValues = array();

$mappedFields = array(
		"d:AddressLine1" => "bill_street",
		"d:City" => "bill_city",
		"d:Email" => "email",
		"d:Fax" => "fax",
		"d:Name" => "accountname",
		"d:Phone" => "phone",
		"d:Postcode" => "bill_code",
		"d:Classification1" => "accounttype",
		"d:CountryName"=>"bill_country"
	);

$code = getConfigValue('account_map_code');
if($code)
	$mappedFields['d:Code'] = $code;

$invoiceTo = getConfigValue('account_map_invoiceto');
if($invoiceTo){
	$mappedFields['d:InvoiceAccountCode'] = $invoiceTo;
	$getCrmIdForValues[] = 'd:InvoiceAccountCode';
}
