<?php

function getConfigValue($var){
	global $adb;
	$configValueQuery = $adb->pquery("select value from vtiger_exact_config where var = ?",array($var));
	$value = $adb->query_result($configValueQuery,0,'value');
	return $value;
}

function getConfigVar($val){
        global $adb;
        $configValueQuery = $adb->pquery("select var from vtiger_exact_config where value = ?",array($val));
        $value = $adb->query_result($configValueQuery,0,'var');
        return $value;
}

function updateConfig($var,$value){
	global $adb;
	$configQuery = $adb->pquery("select value from vtiger_exact_config where var = ?",array($var));
	$configValue = $adb->query_result($configQuery,0,'value');
	if($adb->num_rows($configQuery) != 0)
		$adb->pquery("update vtiger_exact_config set value = ? where var = ?",array($value,$var));
	else
		$adb->pquery("insert into vtiger_exact_config (var,value) values (?,?)",array($var,$value));
}

function getCrmIdWithEOLCode($exactId,$module){
	global $adb;
	$crmIdQuery = $adb->pquery("select crmid from vtiger_exactonline_ids where exactid = ? and module = ? ",array($exactId,$module));
	return $adb->query_result($crmIdQuery,0,'crmid');
}


function getEOLCodeWithCrmId($crmId,$module){
        global $adb;
        $crmIdQuery = $adb->pquery("select exactid from vtiger_exactonline_ids where crmid = ? and module = ? ",array($crmId,$module));
        return $adb->query_result($crmIdQuery,0,'exactid');

}
