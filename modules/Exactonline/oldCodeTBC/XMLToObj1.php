<?php
//$xmlObj = readXML($xml,'splitter_key');
class XMLToObj {
    var $name;  // aa name
    var $symbol;    // three letter symbol
    var $code;  // one letter code
    var $type;  // hydrophobic, charged or neutral

    function XMLToObj ($aa)
    {
        foreach ($aa as $k=>$v)
            $this->$k = $aa[$k];
    }
}
function readXML($data,$splitterKey = "entry")
{
    $parser = xml_parser_create();
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parse_into_struct($parser, $data, $values, $tags);
    xml_parser_free($parser);

    foreach ($tags as $key=>$val) {
        if ($key == $splitterKey) {
            $molranges = $val;
            for ($i=0; $i < count($molranges); $i+=2) {
                $offset = $molranges[$i] + 1;
                $len = $molranges[$i + 1] - $offset;
                $tdb[] = parseValue(array_slice($values, $offset, $len));
            }
        } else {
            continue;
        }
    }
    return $tdb;
}

function parseValue($mvalues)
{
    for ($i=0; $i < count($mvalues); $i++) {
        $mol[$mvalues[$i]["tag"]] = $mvalues[$i]["value"];
    }
    return new XMLToObj($mol);
}

function readSOXML($data){
	//:TODO: Continue here
	$xmlEntries = xml_to_object($data);
	$entries = array();
	foreach($xmlEntries as $xmlEntry){
		foreach($xmlEntry as $entry){
		if($entry->name == "entry"){
			$entries[] = $entry->children;
		}
		}
	}
	$xmlData = array();
	$i = 0;
var_dump(count($entries));
print 'done';
	foreach($entries as $salesorders){
		foreach($salesorders as $entry){
			$xmlData = array();
			$xmlData[$entry->name]['content'] = $entry->content;
			$xmlData[$entry->name]['attributes'] = $entry->attributes;	
			$xmlData[$entry->name]['children'] = $entry->children;
		}
			$xmlDatas[$i] = $xmlData;
			$i++;

print 'done2';
	}
print '<pre>';print 'xmldatas';print_r ($xmlDatas);die;
	return $entries;
}

class XmlElement {
  var $name;
  var $attributes;
  var $content;
  var $children;
};

function xml_to_object($xml) {
  $parser = xml_parser_create();
  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
  xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
  xml_parse_into_struct($parser, $xml, $tags);
  xml_parser_free($parser);

  $elements = array();  // the currently filling [child] XmlElement array
  $stack = array();
  foreach ($tags as $tag) {
    $index = count($elements);
    if ($tag['type'] == "complete" || $tag['type'] == "open") {
      $elements[$index] = new XmlElement;
      $elements[$index]->name = $tag['tag'];
      $elements[$index]->attributes = $tag['attributes'];
      $elements[$index]->content = $tag['value'];
      if ($tag['type'] == "open") {  // push
        $elements[$index]->children = array();
        $stack[count($stack)] = &$elements;
        $elements = &$elements[$index]->children;
      }
    }
    if ($tag['type'] == "close") {  // pop
      $elements = &$stack[count($stack) - 1];
      unset($stack[count($stack) - 1]);
    }
  }
  return $elements[0];  // the single top-level element
}

