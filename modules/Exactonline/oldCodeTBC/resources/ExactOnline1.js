var eolModule = 'Exactonline';
var Vtiger_ExactOnline_Js = {

getFromEOL: function(transferEvent){
        var url = 'index.php?module='+eolModule+"&action=ActionAjax&mode=registerEvent&transferEvent="+transferEvent;
	var thisInstance = this;
	var eventId,responseMessage;
	AppConnector.request(url).then(
                function(data){
			responseMessage = data.result.responseMessage;
			thisInstance.updateResponseMessage(responseMessage);
			eventId = data.result.eventid;
			if(eventId){				
				thisInstance.processDownloadData(eventId);
			}
		}
	);
},

processDownloadData: function(eventId){

        var url = 'index.php?module='+eolModule+"&action=ActionAjax&mode=downloadDataForEvent&eventid="+eventId;
	var thisInstance = this;
	AppConnector.request(url).then(
		function(data){
			thisInstance.updateResponseMessage(data.result.responseMessage);
			console.log(data.result.responseMessage);
		}
	);

},

updateResponseMessage: function(responseMessage){
	jQuery("#showSyncStatusDet").prepend(responseMessage+"<br>");
},

};

jQuery( document ).ready(function() {
	var module = jQuery("#module").val();
	if(module == 'Exactonline'){
		jQuery( ".btn" ).click(function(e) {
			$( ".statusdivs" ).show( "fast" );
			$( ".loadimg" ).show( "fast" );
			Vtiger_ExactOnline_Js.getFromEOL(e.target.id);
		});
	}
});

