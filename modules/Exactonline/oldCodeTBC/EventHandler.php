<?php
require_once "modules/Exactonline/libs/EOL_API.php";
require_once 'modules/Exactonline/DownloadHandler.php';
require_once 'modules/Exactonline/ProcessHandler.php';
require_once 'modules/Exactonline/TransferHandler.php';
require_once "modules/Exactonline/FieldsToSelect.php";

class EventHandler{

	public $eventId,$syncType,$state,$updatedTime;

        const INITIAL = "Initialised";
        const DOWNLOADING = "Downloading";
        const DOWNLOADED = "Downloaded";
        const DOWNLOADFAILED = "Download Failed";
        const PROCESSING = "Processing";
        const PROCESSED = "Processed";

	function __construct($eventId){
		$this->eventId = $eventId;
		$this->setEventDetails();
		$this->doEvent();
	}

	/* Identifies the type of event and handles the event. Splits up the event into processes, and takes care of each process. Updates the status of event.*/
	public function doEvent(){
		$sendSyncTypes = array("send_contacts","send_accounts","send_invoice","send_salesorder","send_products");
		if(!in_array($this->syncType,$sendSyncTypes)){
			if($this->state == self::INITIAL || $this->state == self::DOWNLOADING ){
				$this->updateEventState(self::DOWNLOADING);
				$this->downloadDataForEvent();
				//downloaded state will be updated in download handler
			}
			if($this->state == self::DOWNLOADED || $this->state == self::PROCESSING ){
				$this->updateEventState(self::PROCESSING);
				$this->processDataForEvent();
				$this->updateEventState(self::PROCESSED);
			}
		}else{
			if($this->state == self::INITIAL || $this->state == self::PROCESSING){
				$this->updateEventState(self::PROCESSING);
				$this->transferData();
				$this->updateEventState(self::PROCESSED);
			}
		}
	}

	/* Transfers the event data to ExactONline */

	public function transferData(){
		$transferHandler = new TransferHandler($this->eventId,$this->syncType);
	}

	/* Process data for event */

	public function processDataForEvent(){
		$processHandler = new ProcessHandler($this->eventId,$this->syncType);
	}

	/* Downloads the XML files for the events */

	public function downloadDataForEvent(){
		$downloadHandler = new DownloadHandler($this->eventId,$this->syncType);
		$this->setEventDetails();
		return true;
	}

	/* Updates the status fo the event */

	public function updateEventState($state){
		global $adb;
		$adb->pquery("update vtiger_exact_syncevent set state = ? where synceventid = ?",array($state,$this->eventId));
	}

	/* sets all the event details */

	public function setEventDetails(){
		global $adb;
		$eventQuery = $adb->pquery("select synctype,state,updated_time from vtiger_exact_syncevent where synceventid = ?",array($this->eventId)); 
		$this->syncType = $adb->query_result($eventQuery,0,"synctype");
		$this->state = $adb->query_result($eventQuery,0,"state");
		$this->updatedTime = $adb->query_result($eventQuery,0,"updated_time");
	}

}

?>
