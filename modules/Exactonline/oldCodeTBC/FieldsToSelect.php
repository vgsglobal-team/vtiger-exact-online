<?php

//add here the fields that need to be requeted to exactonline. Refer the field names in //https://developers.exactonline.com/#RestIntro.html%3FTocPath%3DExact%2520Online%2520REST%2520API%7C_____0

$accountModuleFields = array("Accountant","Division","Created","Modified","ID","Accountant","AddressLine1","AddressLine2","AddressLine3","City","Code","Country","DiscountSales","Email","Fax","InvoiceAccountCode","Name","Phone","Postcode","SearchCode","Status");
//$productModuleFields = array("Code","Description","SalesVatCode","SalesVatCodeDescription","SalesPrice");
$productModuleFields = array("ID","Account","Code","Description","SalesPrice","SalesVatCode","SalesVatCodeDescription","Stock","UnitDescription");

$salesOrderModuleFields = array("Description,DeliveryDate,YourRef,AmountDC","DeliverTo","DeliverToContactPerson","DeliverToContactPersonFullName","InvoiceTo","OrderDate","OrderedBy","OrderNumber","PaymentCondition","SalesOrderLines","Salesperson","Status","StatusDescription","WarehouseCode","WarehouseID");

$processLastMinutes = 5;
