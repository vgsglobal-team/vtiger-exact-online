<?php
/** License Text Here **/
class Exactonline {
	/**
	 * Invoked when special actions are performed on the module.
	 * @param String Module name
	 * @param String Event Type (module.postinstall, module.disabled, module.enabled, module.preuninstall)
	 */
	function vtlib_handler($modulename, $event_type) {
		if($event_type == 'module.postinstall') {
//			$this->createTables();

			// TODO Handle post installation actions
		} else if($event_type == 'module.disabled') {
			// TODO Handle actions when this module is disabled.
		} else if($event_type == 'module.enabled') {
			// TODO Handle actions when this module is enabled.
		} else if($event_type == 'module.preuninstall') {
			// TODO Handle actions when this module is about to be deleted.
		} else if($event_type == 'module.preupdate') {
			// TODO Handle actions before this module is updated.
		} else if($event_type == 'module.postupdate') {
			// TODO Handle actions after this module is updated.
			$this->createTables();
				$this->addJSLinks();
	}
	}


        public function createTables() {
                global $adb;
                $tables = $this->collectTables();
                foreach ($tables as $table => $createTableQuery) {
                        $tableExist = $this->checkTableExists($table);
                        if (!$tableExist) {
                                $adb->pquery($createTableQuery, array());
                        } else {
				//table already exists
                        }
                }
        }

        public function checkTableExists($tableName) {
                global $adb;
                $chkTQuery = $adb->pquery("show tables like ?", array($tableName));
                $chkT = $adb->num_rows($chkTQuery);
                return $chkT;
        }

        public function collectTables() {

                /* To add an table while installing the product, just add a value
                        $table['tablename'] = 'create table query';
                */
                $tables['vtiger_exact_config'] = 'CREATE TABLE vtiger_exact_config(
                                id int(11) NOT NULL AUTO_INCREMENT,
                                var varchar(255) NOT NULL,
                                value longtext NOT NULL,
                                PRIMARY KEY (id)
                                ) ENGINE=MyISAM';

		$tables['vtiger_exact_event'] = 'Create TABLE vtiger_exact_event( eventid int(11) NOT NULL AUTO_INCREMENT, time datetime,type varchar(255),status varchar(255),module varchar(255), PRIMARY KEY(eventid))';

		$tables['vtiger_exactonline_ids'] = 'CREATE TABLE vtiger_exactonline_ids (id int(10) NOT NULL AUTO_INCREMENT, exactid varchar(100) NOT NULL, crmid int(10) NOT NULL,module varchar(100) DEFAULT NULL, source varchar(100) DEFAULT NULL, PRIMARY KEY (id)) ENGINE=InnoDB';
		return $tables;
	}

        public function addJSLinks(){
                global $log;
                $tabId = 0;
                $headerScriptLinkType = 'HEADERSCRIPT';
                $incominglinkLabel = "Exactonline";
                $handlerInfo = array('path' => 'modules/Exactonline/Exactonline.php',
                                'class' => 'Exactonline',
                                'method' => 'checkLinkPermission');
                Vtiger_Link::addLink($tabId, $headerScriptLinkType, $incominglinkLabel, 'modules/Exactonline/resources/ExactOnline.js','','',$handlerInfo);
        }

        public function checkLinkPermission($linkData){
                $module = new Vtiger_Module();
                $moduleInstance = $module->getInstance('Exactonline');
                if($moduleInstance) {
                        return true;
                }else {
                        return false;
                }
        }

}
