<?php
require_once 'modules/Exactonline/VtImporter.php';
class ProcessHandler{

	const DOWNLOADED = "Downloaded";
	const PROCESSING = "Processing";
	const PROCESSED = "Processed";

	public $eventId,$syncType,$downloadId = false,$xmlUrl,$processingIndex = 0;

	function __construct($eventId, $syncType){
		$this->eventId = $eventId;
		$this->syncType = $syncType;
		$this->initiateProcess();
	}

	public function initiateProcess(){
		do{
			$this->downloadId = false;
			$this->setDownloadId();
			if($this->downloadId){
				$this->initiateProcessForDownloadId();
			}
		}while($this->downloadId);
	}

	public function initiateProcessForDownloadId(){
		$this->updateProcessingState(self::PROCESSING);
		$data  = $this->readXmlData();
		$this->processData($data);
		$this->updateProcessingState(self::PROCESSED);
	}

	public function processData($data){
		$eolModule = $this->getModuleForSyncType();
		$vtigerModule = $this->getVtigerModule();
		if($vtigerModule == "Accounts")
			$this->addAccounts($data);
		elseif($vtigerModule == "Products")
			$this->addProducts($data);
		elseif($vtigerModule == "SalesOrder")
			$this->addSalesOrders($data);
	}

	public function addSalesOrders($salesOrdersData){
		var_dump(count($salesOrdersData['feed']['entry']));
		$getFromEOL = true;
		require 'modules/Exactonline/salesOrderSyncMapper.php';
		foreach($salesOrdersData['feed']['entry'] as $salesOrder){
		}
		$i = 1;
		print '<pre>';
//		print_r ($salesOrdersData);
//die;
$salesOrdersArrangedData = array();
		foreach($salesOrdersData as $salesOrderData){
print_r($salesOrderData);
die('asasaasa');
print "<br>******************************************************************************<br>";
}die;

{
			if($this->processingIndex <= $i){
				$salesOrderData = (array)$salesOrderData;
				$salesOrderLine = new EOL_API('SalesOrderLines');
				$response = $salesOrderLine->get(array('$filter'=>"ID eq guid'eca30ffd-b50b-4f51-a73d-ca923a229e79'",'$select'=>'OrderID,OrderNumber,Item,ItemCode,LineNumber,Quantity,QuantityDelivered,QuantityInvoiced'));

print '<pre>';var_dump (readXML($response));
				print '<pre>';print_r ($salesOrderData);
				print_r ($r);
			}
			die;
			$i++;
		}
		die('premasasasas');
		print '<pre>';print_r ($salesOrdersData);die('prem');

	}
	public function addProducts($productsData){
		$getFromEOL = true;
		require 'modules/Exactonline/itemSyncMapper.php';
		$i = 1;
		foreach($productsData as $productData){
			if($this->processingIndex <= $i){
				$productData = (array) $productData;
				foreach($mappedFields as $key => $value){
					if($key == "d:SalesVatCode")
						$productData[$key] = getConfigVar("eol_vat_code_val_".$productData[$key]);
					$vtigerArrangedData[$value] = $productData[$key];
				}
				$this->addDataToVtiger("Products",$vtigerArrangedData,$productData['d:ID']);
			}
			$i++;
		}
	}

	public function addAccounts($accountsData){

		require 'modules/Exactonline/accountSyncMapper.php';
		$i = 1;
		foreach($accountsData as $accountData){
			if($this->processingIndex <= $i){
				$accountData = (array)$accountData;
				foreach($mappedFields as $key => $value){
					if(in_array($key,$getCrmIdForValues)){
						$accountData[$key] = getCrmIdWithEOLCode($accountData[$key],"Accounts");
					}
					//:TODO May be rework needed
					if($key == "d:Classification1"){
						$accountData[$key] = str_replace("eoonline_classification_code","",getConfigVar($accountData[$key]));
					}
					$vtigerArrangedData[$value] = $accountData[$key];
				}
				$this->addDataToVtiger("Accounts",$vtigerArrangedData,$accountData['d:ID']);
				$this->updateProcessingIndex($i);
			}
			$i++;
		}
	}

	public function updateProcessingIndex($processingIndex){
		global $adb;
		$adb->pquery("update vtiger_exact_download set processingindex = ? where downloadid = ?",array($processingIndex,$this->downloadId));
	}

	public function addDataToVtiger($moduleName,$moduleData,$uniqueId){
		$vtImporter = new VtImporter($moduleName);
		$vtImporter->setEolId($uniqueId);
		$vtImporter->setAllColumnFields($moduleData);
		$vtImporter->save();
		return $vtImporter->crmId;
	}

	public function getVtigerModule(){
		if($this->syncType == "get_accounts")
			return "Accounts";
		if($this->syncType == "get_products")
			return "Products";
		if($this->syncType == "get_salesorder")
			return "SalesOrder";
	}

	public function getModuleForSyncType(){
		if($this->syncType == "get_accounts")
			return "Accounts";
		if($this->syncType == "get_products")
			return "Items";
		if($this->syncType == "get_salesorder")
			return "SalesOrders";
		if($this->syncType == "get_invoice")
			return "Invoices";
	}

	public function readXmlData(){
		global $site_URL;
		if($this->syncType == "get_salesorder")
			return xmlToArray(simplexml_load_file($site_URL.$this->xmlUrl));
		return readXML(file_get_contents($site_URL.$this->xmlUrl));
	}

	public function setDownloadId(){
		global $adb;
		$downloadIdQuery = $adb->pquery("select downloadid,xmlurl,processingindex from vtiger_exact_download where synceventid = ? and state = ?",array($this->eventId,self::PROCESSING));
		if($adb->num_rows($downloadIdQuery) != 0){
			$this->downloadId = $adb->query_result($downloadIdQuery,0,'downloadid');
			$this->xmlUrl = $adb->query_result($downloadIdQuery,0,'xmlurl');
			$this->processingIndex = $adb->query_result($downloadIdQuery,0,'processingindex');
		}else{
			$downloadIdQuery = $adb->pquery("select downloadid,xmlurl,processingindex from vtiger_exact_download where synceventid = ? and state = ? order by downloadid asc limit 1",array($this->eventId,self::DOWNLOADED));
			if($adb->num_rows($downloadIdQuery) != 0){
				$this->downloadId = $adb->query_result($downloadIdQuery,0,'downloadid');
				$this->xmlUrl = $adb->query_result($downloadIdQuery,0,'xmlurl');
				$this->processingIndex = $adb->query_result($downloadIdQuery,0,'processingindex');
			}
		}

	}


	public function updateProcessingState($state){
		global $adb;
		$adb->pquery("update vtiger_exact_download set state = ? where downloadid = ?",array($state,$this->downloadId));
	}

}
