<?php
require_once 'modules/Exactonline/XMLToObj.php';
require_once 'modules/Exactonline/commonFns.php';
class EOL_API{

        private $clientId = null;
        private $clientSecret = null;
        private $division = null;
        private $exactApiUrl = "https://start.exactonline.nl";
        private $accessToken = null;
        private $code = null;
        public $access_token = null;
        private $refresh_token = null;
        public $fields = array('clientid','clientsecret','division');
	private $accountUrl,$itemUrl,$salesOrderUrl,$invoiceUrl,$classificationUrl,$userUrl,$warehouseUrl,$vatCodeUrl,$costCenterUrl,$paymentConditionsUrl,$salesOrderLinesUrl;
	public $module;
	public $exceptionRaised = false,$response,$message;

        public function __construct( $module = null){
                $this->setCredentials();
		$this->module = $module;
        }

        public function setCredentials(){
                $this->clientId = $this->getConfigValue('clientid');
                $this->clientSecret = $this->getConfigValue('clientsecret');
                $this->division = $this->getConfigValue('division');
                $this->code = $this->getConfigValue('code');
                if($this->code == null){
                        $this->getCode();
		}
		$this->access_token = $this->getConfigValue('access_token');
		$this->refresh_token = $this->getConfigValue('refresh_token');
		$this->accountUrl = $this->exactApiUrl."/api/v1/".$this->division."/crm/Accounts";
		$this->itemUrl = $this->exactApiUrl."/api/v1/".$this->division."/read/logistics/Items";
		$this->salesOrderUrl = $this->exactApiUrl."/api/v1/".$this->division."/salesorder/SalesOrders";
		$this->invoiceUrl = $this->exactApiUrl."/api/v1/".$this->division."/salesinvoice/SalesInvoices";
		$this->classificationUrl = $this->exactApiUrl."/api/V1/".$this->division."/crm/AccountClassifications";
		$this->userUrl = $this->exactApiUrl."/api/V1/".$this->division."/users/Users";
		$this->warehouseUrl = $this->exactApiUrl."/api/V1/".$this->division."/inventory/Warehouses";
		$this->vatCodeUrl = $this->exactApiUrl."/api/V1/".$this->division."/vat/VATCodes";
		$this->costCenterUrl = $this->exactApiUrl."/api/V1/".$this->division."/hrm/Costcenters";
		$this->paymentConditionsUrl = $this->exactApiUrl."/api/V1/".$this->division."/cashflow/PaymentConditions";
		$this->salesOrderLinesUrl = $this->exactApiUrl."/api/V1/".$this->division."/salesorder/SalesOrderLines";
	}

        public function getConfigValue($var){
                global $adb;
                $getConfigQuery = $adb->pquery("select value from vtiger_exact_config where var = ?",array($var));
                return $adb->query_result($getConfigQuery,0,'value');
        }

        public function getCode(){
                global $site_URL;
                $redirect_uri = $site_URL."index.php?module=Exactonline&view=Credentail";
                $api_request_parameters = array(
                                'client_id' => $this->clientId,
                                'client_secret' => $this->clientSecret,
                                'redirect_uri' => $redirect_uri,
                                'response_type' => 'code'
                                );
                $this->exactApiUrl .= '/api/oauth2/auth?' . http_build_query($api_request_parameters);
                header('Location: '.$this->exactApiUrl);
                die('done');
        }

        public function refreshAccessToken(){
                global $site_URL;
                $this->access_token = $this->getConfigValue('access_token');
                $this->refresh_token = $this->getConfigValue('refresh_token');
                $postArray = array(
                        'refresh_token' => $this->refresh_token,
                        'grant_type' => 'refresh_token',
                        'client_id' => $this->clientId,
                        'client_secret' => $this->clientSecret
                        );
                $url = $this->exactApiUrl.'/api/oauth2/token';
                $token = $this->postWithCurl($url,$postArray);
                if(isset($token->access_token)){
                        $this->access_token = $token->access_token;
                        return $token->access_token;
                }
                return false;
        }

        public function getAccessToken(){

                global $site_URL;
                $url = $this->exactApiUrl.'/api/oauth2/token';
                $postArray = array(
                        'code' => $this->code,
                        'client_id' => $this->clientId,
                        'client_secret' => $this->clientSecret,
                        'grant_type' => 'authorization_code',
                        'redirect_uri' => $site_URL."index.php?module=Exactonline&view=Credentail"
                );
                return $this->postWithCurl($url,$postArray);
        }


	function postWithCurl($url, $fields)
	{
//error_log(json_encode($fields),3,"logs/eol.log");
		$post_field_string = http_build_query($fields, '', '&');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field_string);
//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		curl_setopt($ch, CURLOPT_POST, true);
		$response = curl_exec($ch);
		curl_close ($ch);
		$decoded_response = json_decode($response);
		if($response && !isset($decoded_response->error) && isset($decoded_response)){
			return $decoded_response;
		}else{
			$this->exceptionRaised = true;
			$this->response = $response;
			$this->message = "Exception raised when posting to $url\n"."Posted data are".json_encode($fields);
		}
		return false;
	}

	function getUrl(){
		if($this->module == "Accounts")
			return $this->accountUrl;
		if($this->module == "Items")
			return $this->itemUrl;
		if($this->module == "SalesOrders")
			return $this->salesOrderUrl;
		if($this->module == "Invoices")
			return $this->invoiceUrl;
		if($this->module == "Classifications")
			return $this->classificationUrl;
		if($this->module == "Users")
			return $this->userUrl;
		if($this->module == "Warehouses")
			return $this->warehouseUrl;
		if($this->module == "Vatcodes")
			return $this->vatCodeUrl;
		if($this->module == "Costcenters")
			return $this->costCenterUrl;
		if($this->module == "Paymentconditions")
			return $this->paymentConditionsUrl;
		if($this->module == "SalesOrderLines")
			return $this->salesOrderLinesUrl;
	}

	function get($params=array(),$url = false)
	{
		if($url == false){
			$url = $this->getUrl();
			$params['access_token'] = $this->refreshAccessToken();
			$url = $url.'?'.http_build_query($params, '', '&');
		}else{
			$params['access_token'] = $this->refreshAccessToken();
			$url .= "&".http_build_query($params,'','&');
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}
}


