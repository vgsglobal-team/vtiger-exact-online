<?php
require_once 'modules/Exactonline/VtImporter.php';
class ProcessHandler{

	const DOWNLOADED = "Downloaded";
	const PROCESSING = "Processing";
	const PROCESSED = "Processed";

	public $eventId,$syncType,$downloadId = false,$xmlUrl,$processingIndex = 0;

	function __construct($eventId, $syncType){
		$this->eventId = $eventId;
		$this->syncType = $syncType;
		$this->initiateProcess();
	}

	public function initiateProcess(){
		do{
			$this->downloadId = false;
			$this->setDownloadId();
			if($this->downloadId){
				$this->initiateProcessForDownloadId();
			}
		}while($this->downloadId);
	}

	public function initiateProcessForDownloadId(){
		$this->updateProcessingState(self::PROCESSING);
		$data  = $this->readXmlData();
		$this->processData($data);
		$this->updateProcessingState(self::PROCESSED);
	}

	public function processData($data){
		$eolModule = $this->getModuleForSyncType();
		$vtigerModule = $this->getVtigerModule();
		if($vtigerModule == "Accounts")
			$this->addAccounts($data);
		elseif($vtigerModule == "Products")
			$this->addProducts($data);
		elseif($vtigerModule == "SalesOrder")
			$this->addSalesOrders($data);
		elseif($vtigerModule == "Invoice")
			$this->addInvoice($data);
	}

	public function addInvoice($invoicesData){
		$getFromEOL = true;
		require "modules/Exactonline/invoiceSyncMapper.php";
		$i = 1;
		foreach($invoicesData['feed']['entry'] as $invoice){
			if($this->processingIndex <= $i){
				$invoiceData = $this->refineSalesOrderData($invoice);
				$invoiceData['d:InvoiceID'] = $invoice['content']['m:properties']['d:InvoiceID']['value'];
				foreach($mappedFields as $key => $value){
                                        if($key == "d:OrderedBy")
                                                $invoiceData[$key] = getCrmIdWithEOLCode($invoiceData[$key],"Accounts");
                                        if($key == "d:OrderedByContactPerson")
                                                $invoiceData[$key] = getCrmIdWithEOLCode($invoiceData[$key],"Contacts");
                                        $vtigerArrangedData[$value] = $invoiceData[$key];
				}
				$vtigerArrangedData['lineItems'] = $invoiceData['lineItems'];
				$this->addDataToVtiger("Invoice",$vtigerArrangedData,$vtigerArrangedData['orderEOLId']);

			}
			$i++;
		}
	}

	public function addSalesOrders($salesOrdersData){
		$getFromEOL = true;
		require 'modules/Exactonline/salesOrderSyncMapper.php';
		$i = 1;
		foreach($salesOrdersData['feed']['entry'] as $salesOrder){
			if($this->processingIndex <= $i){
				$salesOrderData = $this->refineSalesOrderData($salesOrder);
				foreach($mappedFields as $key => $value){
					if($key == "d:OrderedBy")
						$salesOrderData[$key] = getCrmIdWithEOLCode($salesOrderData[$key],"Accounts");
					if($key == "d:OrderedByContactPerson")
						$salesOrderData[$key] = getCrmIdWithEOLCode($salesOrderData[$key],"Contacts");
					$vtigerArrangedData[$value] = $salesOrderData[$key];
				}
				$vtigerArrangedData['lineItems'] = $salesOrderData['lineItems'];
				$this->addDataToVtiger('SalesOrder',$vtigerArrangedData,$vtigerArrangedData['orderEOLId']);
			}
			$i++;
		}
	}


	public function addProducts($productsData){
		$getFromEOL = true;
		require 'modules/Exactonline/itemSyncMapper.php';
		$i = 1;
		foreach($productsData as $productData){
			if($this->processingIndex <= $i){
				$productData = (array) $productData;
				foreach($mappedFields as $key => $value){
					if($key == "d:SalesVatCode")
						$productData[$key] = getConfigVar("eol_vat_code_val_".$productData[$key]);
					$vtigerArrangedData[$value] = $productData[$key];
				}
				$this->addDataToVtiger("Products",$vtigerArrangedData,$productData['d:ID']);
			}
			$i++;
		}
	}

	public function addAccounts($accountsData){

		require 'modules/Exactonline/accountSyncMapper.php';
		$i = 1;
		foreach($accountsData as $accountData){
			if($this->processingIndex <= $i){
				$accountData = (array)$accountData;
				foreach($mappedFields as $key => $value){
					if(in_array($key,$getCrmIdForValues)){
						$accountData[$key] = getCrmIdWithEOLCode($accountData[$key],"Accounts");
					}
					//:TODO May be rework needed
					if($key == "d:Classification1"){
						$accountData[$key] = str_replace("eoonline_classification_code","",getConfigVar($accountData[$key]));
					}
					$vtigerArrangedData[$value] = $accountData[$key];
				}
				$this->addDataToVtiger("Accounts",$vtigerArrangedData,$accountData['d:ID']);
				$this->updateProcessingIndex($i);
			}
			$i++;
		}
	}

	public function updateProcessingIndex($processingIndex){
		global $adb;
		$adb->pquery("update vtiger_exact_download set processingindex = ? where downloadid = ?",array($processingIndex,$this->downloadId));
	}

	public function addDataToVtiger($moduleName,$moduleData,$uniqueId){
		$vtImporter = new VtImporter($moduleName);
		$vtImporter->setEolId($uniqueId);
		$vtImporter->setAllColumnFields($moduleData);
		$vtImporter->save();
		$inventoryModules = array('SalesOrder','Invoice');
		if(in_array($moduleName,$inventoryModules)){
			$vtImporter->setInventoryRelations($moduleData['lineItems']);
		}
		return $vtImporter->crmId;
	}

	public function getVtigerModule(){
		if($this->syncType == "get_accounts")
			return "Accounts";
		if($this->syncType == "get_products")
			return "Products";
		if($this->syncType == "get_salesorder")
			return "SalesOrder";
		if($this->syncType == "get_invoice")
			return "Invoice";
	}

	public function getModuleForSyncType(){
		if($this->syncType == "get_accounts")
			return "Accounts";
		if($this->syncType == "get_products")
			return "Items";
		if($this->syncType == "get_salesorder")
			return "SalesOrders";
		if($this->syncType == "get_invoice")
			return "Invoices";
	}

	public function readXmlData(){
		global $site_URL;
		if($this->syncType == "get_salesorder" || $this->syncType == "get_invoice")
			return xmlToArray(simplexml_load_file($site_URL.$this->xmlUrl));
		return readXML(file_get_contents($site_URL.$this->xmlUrl));
	}

	public function setDownloadId(){
		global $adb;
		$downloadIdQuery = $adb->pquery("select downloadid,xmlurl,processingindex from vtiger_exact_download where synceventid = ? and state = ?",array($this->eventId,self::PROCESSING));
		if($adb->num_rows($downloadIdQuery) != 0){
			$this->downloadId = $adb->query_result($downloadIdQuery,0,'downloadid');
			$this->xmlUrl = $adb->query_result($downloadIdQuery,0,'xmlurl');
			$this->processingIndex = $adb->query_result($downloadIdQuery,0,'processingindex');
		}else{
			$downloadIdQuery = $adb->pquery("select downloadid,xmlurl,processingindex from vtiger_exact_download where synceventid = ? and state = ? order by downloadid asc limit 1",array($this->eventId,self::DOWNLOADED));
			if($adb->num_rows($downloadIdQuery) != 0){
				$this->downloadId = $adb->query_result($downloadIdQuery,0,'downloadid');
				$this->xmlUrl = $adb->query_result($downloadIdQuery,0,'xmlurl');
				$this->processingIndex = $adb->query_result($downloadIdQuery,0,'processingindex');
			}
		}

	}


	public function updateProcessingState($state){
		global $adb;
		$adb->pquery("update vtiger_exact_download set state = ? where downloadid = ?",array($state,$this->downloadId));
	}

	public function refineInvoiceData($invoice){
print '<pre>';print_r ($invoice);die;

	}

	public function refineSalesOrderData($salesOrder){
		$salesOrderData = array(
			"d:OrderedBy"=> $salesOrder['content']['m:properties']['d:OrderedBy']['value'],
			"d:OrderedByContactPerson" => $salesOrder['content']['m:properties']['d:OrderedByContactPerson']['value'],
			"d:OrderNumber" => $salesOrder['content']['m:properties']['d:OrderNumber']['value'],
			"d:Description" => $salesOrder['content']['m:properties']['d:YourRef'],
			"d:Remarks" => $salesOrder['content']['m:properties']['d:Remarks']['value'],
			"d:Salesperson" => $salesOrder['content']['m:properties']['d:Salesperson']['value'],
			"d:StatusDescription" => $salesOrder['content']['m:properties']['d:StatusDescription'],
			"d:OrderID" => $salesOrder['content']['m:properties']['d:OrderID']['value']
		);
		$salesOrderItems = $salesOrder['link'][1]['m:inline']['feed']['entry'];
		$productIds = array();	
		$lineItems = array();
		$i = 0;
		if(isset($salesOrderItems[0])){
			foreach($salesOrderItems as $salesOrderItem){
				$lineItems['product_ids'][$i] = getCrmIdWithEOLCode($salesOrderItem['content']['m:properties']['d:Item']['value'],'Products');
				$lineItems['quantity'][$i] = $salesOrderItem['content']['m:properties']['d:Quantity']['value'];
				$lineItems['discount'][$i] = $salesOrderItem['content']['m:properties']['d:Discount']['value'];
				$lineItems['total_price'][$i] = $salesOrderItem['content']['m:properties']['d:AmountDC']['value'];
				$lineItems['vat_amount'][$i] = $salesOrderItem['content']['m:properties']['d:VATAmount']['value'];
				$i++;
			}
		}else{
			$salesOrderItem = $salesOrderItems;
			$lineItems['product_ids'][$i] = getCrmIdWithEOLCode($salesOrderItem['content']['m:properties']['d:Item']['value'],'Products');
			$lineItems['quantity'][$i] = $salesOrderItem['content']['m:properties']['d:Quantity']['value'];
			$lineItems['discount'][$i] = $salesOrderItem['content']['m:properties']['d:Discount']['value'];
			$lineItems['total_price'][$i] = $salesOrderItem['content']['m:properties']['d:AmountDC']['value'];
			$lineItems['vat_amount'][$i] = $salesOrderItem['content']['m:properties']['d:VATAmount']['value'];

		}
		$salesOrderData['lineItems'] = $lineItems;
		return $salesOrderData;
	}
}
