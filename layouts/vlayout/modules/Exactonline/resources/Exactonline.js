jQuery.Class("ExactOnline_Js", {}, {
    fade_out: function() {
        $("#eol_result").fadeOut("slow").empty().removeClass('alert alert-danger').removeClass('alert alert-success');
    },
    testConnections: function() {
        jQuery('.test').on('click', function(e) {
            var progressIndicatorElement = jQuery.progressIndicator();
            var dataUrl = "index.php?module=Exactonline&action=TryConnectionsAjax";
            AppConnector.request(dataUrl).then(function(data) {
                progressIndicatorElement.progressIndicator({
                    'mode': 'hide'
                });
                if (data.success) {
                    var conn_result = data.result;
                    if (conn_result.status == 'ok') {
                        $('#eol_result').html(conn_result.message);
                        $('#eol_result').addClass('alert alert-success');
                        $('#eol_result').css('margin-top', '2%');
                        $('#eol_result').css('margin-left', '0%');
                    } else {
                        $('#eol_result').html(conn_result.message);
                        $('#eol_result').addClass('alert alert-danger');
                        $('#eol_result').css('margin-top', '2%');
                        $('#eol_result').css('margin-left', '0%');
                    }
                    $('#eol_result').fadeIn();
                    var instance = new ExactOnline_Js();
                    setTimeout(instance.fade_out, 5000);
                }
            }, function(error, err) {});
            progressIndicatorElement.progressIndicator({
                'mode': 'hide'
            })
        });
    },
    syncNow: function() {
        jQuery('.syncnow').on('click', function(e) {
            var progressIndicatorElement = jQuery.progressIndicator();
            var dataUrl = "index.php?module=Exactonline&action=SyncNow";
            AppConnector.request(dataUrl).then(function(data) {
                if (data.success) {
                    progressIndicatorElement.progressIndicator({
                        'mode': 'hide'
                    });
                    var conn_result = data.result;
                    $('#eol_result').html(conn_result);
                    $('#eol_result').addClass('alert alert-success');
                    $('#eol_result').css('margin-top', '2%');
                    $('#eol_result').css('margin-left', '0%');
                    
                    $('#eol_result').fadeIn();
                    var instance = new ExactOnline_Js();
                    setTimeout(instance.fade_out, 5000);
                }
            }, function(error, err) {});
        });
    },
    registerEvents: function () {
        this.testConnections();
        this.syncNow();
    }
});

jQuery(document).ready(function () {
    var instance = new ExactOnline_Js();
    instance.registerEvents();
});