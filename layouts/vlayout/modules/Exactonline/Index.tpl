<script>
    jQuery( document ).ready(function() {
        jQuery( "#revoke" ).click(function() {
            jQuery("#isrevoked").val("true");
        });
    });
</script>

<div class="bodyContents" style="min-height: 263px;padding:0 0;">
    <div class="mainContainer row-fluid" style="min-height: 550px;margin-top:0px;">
        <div id="leftPanel" class="span2 row-fluid " style="min-height:550px;">
            {include file="Sideb.tpl"|vtemplate_path:$MODULE}
        </div>
        <div id="rightPanel" class="contentsDiv span10 marginLeftZero" style="min-height: 880px;">
            <div style="width: 65%;margin: auto;margin-top: 2em;padding: 2em;">
                <form id="eol-config" class="form-horizontal" data-detail-url="{$detail_url}" action="{$action_url}" method="post">
                    <input type = "hidden" name = "isrevoked" value = "" id = "isrevoked">
                    <div class="widget_header row-fluid">
                        <div><!--<div class="span8">-->
                            <div class="span4">
                                <h3>{$MODULE} - Sync Module</h3>
                            </div>
                            <div class="span4 btn-toolbar pull-right">
                                <div class="pull-right">
                                    <button title="Revoke" name = "revoke" id = "revoke" class="btn btn-success saveButton"><strong>Revoke</strong></button>
                                    <button title="Save" type="submit" class="btn btn-success saveButton"><strong>Save</strong></button>
                                    <!--<a title="Cancel" class="cancelLink" type="reset">Cancel</a>-->
                                </div>
                            </div>
                            <table class="table table-bordered table-condensed themeTableColor">
                                <thead>
                                    <tr class="blockHeader"><th class="medium" colspan="2">{$MODULE} Oauth Credentials</th></tr>
                                </thead>
                                <tbody>
                                    <tr><td width="20%" class="medium"><label class="muted pull-right marginRight10px"><span class="redColor">* </span>Client Id</label></td><td style="border-left: none;" class="medium"><input type="text" value="{$clientid}" data-validation-engine="validate[required]" name="clientid" style="width: 50%;"></td></tr>
                                    <tr><td class="medium"><label class="muted pull-right marginRight10px"><span class="redColor">* </span>Client Secret</label></td><td style="border-left: none;" class="medium"><input type="text" td="" value="{$clientsecret}" name="clientsecret" style="width: 50%;"></td></tr>
                                    <tr><td class="medium"><label class="muted pull-right marginRight10px">Division</label></td><td style="border-left: none;" class="medium"><input type="text" value="{$division}" name="division" style="width: 50%;"></td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
                <div>
                    <button class="btn test" style="margin-top: 0.5em;">Test Connection</button>
                    <button class="btn syncnow" style="margin-top: 0.5em;">Synch Now</button>
                </div>
                <div id="eol_result"></div>            
            </div>
        </div>
    </div>
</div>

