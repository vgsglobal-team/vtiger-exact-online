<script>
    $(document).ready(function () {
        $('#datepicker').datepicker();
    });
</script>
<style>
    .equalSplit{
        margin-top:10px;
    }
</style>
<div class="bodyContents" style="min-height: 263px;padding:0 0;">
    <div class="mainContainer row-fluid" style="min-height: 550px;margin-top:0px;">
        <div id="leftPanel" class="span2 row-fluid " style="min-height:550px;">
            {include file="Sideb.tpl"|vtemplate_path:$MODULE}
        </div>
        <div id="rightPanel" class="contentsDiv span10 marginLeftZero" style="min-height:550px;">
            <div class="container-fluid">
                <div class="widget_header row-fluid">
                    <div class="span12">
                        <form id="eol-config" class="form-horizontal" data-detail-url="{$detail_url}" action="{$action_url}" method="post">
                            <div class="span12">
                                <div class="span4 marginLeftZero">
                                    <h3>{$MODULE} Configurations</h3>
                                </div>
                                <div class="span4 pull-right">
                                    <div class="pull-right"><button title="Save" type="submit" class="btn btn-success saveButton"><strong>Save</strong></button></div>
                                </div>
                            </div>
                            <div class="span12">
                                <table class="table table-bordered equalSplit detailview-table">
                                    <thead>
                                        <tr>
                                            <th class="blockHeader" colspan="4">SalesOrder Sync configuration</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Warehouse</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 span2 select100" name="salesorder_warehouse">
                                                            {foreach key = left item=value from = $WAREHOUSES}
                                                                <option value="{$value.value}" {if $salesorder_warehouse eq $value.value} selected {/if}> {$value.label} </option>
                                                            {/foreach}
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Default VAT Code</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 span2 select100" name="salesorder_dafaultvatcode">
                                                            {foreach key = left item=value from = $VATCODES}
                                                                <option value="{$value.value}" {if $salesorder_dafaultvatcode eq $value.value} selected {/if}> {$value.label} </option>
                                                            {/foreach}
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Cost Center</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 span2 select100" name="salesorder_costcenter">
                                                            {foreach key = left item=value from = $COSTCENTERS}
                                                                <option value="{$value.value}" {if $salesorder_costcenter eq $value.value} selected {/if}> {$value.label} </option>
                                                            {/foreach}
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px"> Sync SalesOrder</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 span2 select100" name="salesorder_syncstate">
                                                            <option value="1" {if $salesorder_syncstate eq "1"} selected {/if}> Yes</option>
                                                            <option value="0" {if $salesorder_syncstate eq "0"} selected {/if}> No</option>
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Get Status From EOL</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 span2 select100" name="salesorder_getstatus">
                                                            <option value="1" {if $salesorder_getstatus eq "1"} selected {/if}> Yes</option>
                                                            <option value="0" {if $salesorder_getstatus eq "0"} selected {/if}> No</option>
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table table-bordered equalSplit detailview-table">
                                    <thead>
                                        <tr>
                                            <th class="blockHeader" colspan="4">SalesOrder Mapping</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Description</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 span2 select100" name="salesorder_map_description">
                                                            {foreach key = left item=value from = $salesOrderFields}
                                                                {if $value.uitype eq 1 or $value.uitype eq 2 or $value.uitype eq 15 or $value.uitype eq 16 or $value.uitype eq 19}
                                                                    <option value="{$value.columnname}" {if $salesorder_map_description eq $value.columnname} selected {/if}> {$value.fieldlabel} </option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Order Number</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 span2 select100" name="salesorder_map_ordernumber">
                                                            {foreach key = left item=value from = $salesOrderFields}
                                                                {if $value.uitype eq 1 or $value.uitype eq 2 or $value.uitype eq 15 or $value.uitype eq 16 or $value.uitype eq 19}
                                                                    <option value="{$value.columnname}" {if $salesorder_map_ordernumber eq $value.columnname} selected {/if}> {$value.fieldlabel} </option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Order Date</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 span2 select100" name="salesorder_map_orderdate">
                                                            {foreach key = left item=value from = $salesOrderFields}
                                                                {if $value.uitype eq 5}
                                                                    <option value="{$value.columnname}" {if $salesorder_map_orderdate eq $value.columnname} selected {/if}> {$value.fieldlabel} </option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>

                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Delivery Date</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 span2 select100" name="salesorder_map_deliverdate">
                                                            {foreach key = left item=value from = $salesOrderFields}
                                                                {if $value.uitype eq 5}
                                                                    <option value="{$value.columnname}" {if $salesorder_map_deliverdate eq $value.columnname} selected {/if}> {$value.fieldlabel} </option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Your Ref</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 span2 select100" name="salesorder_map_yourref">
                                                            {foreach key = left item=value from = $salesOrderFields}
                                                                {if $value.uitype eq 1 or $value.uitype eq 2 or $value.uitype eq 15 or $value.uitype eq 16 or $value.uitype eq 19}
                                                                    <option value="{$value.columnname}" {if $salesorder_map_yourref eq $value.columnname} selected {/if}> {$value.fieldlabel} </option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Payment Condition</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {$salesorder_map_payment_condition}
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Create Values for PC</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <input type="checkbox" name="salesorder_map_cvalues4pc" {if $salesorder_map_cvalues4pc eq "on" }checked="checked"{/if}>
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Pull Status from EOL</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <input type="checkbox" name="salesorder_map_pullstatusfromeol" {if $salesorder_map_pullstatusfromeol eq "on" }checked="checked"{/if}>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Push Status to EOL</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <input type="checkbox" name="salesorder_map_pushstatustoeol" {if $salesorder_map_pushstatustoeol eq "on" }checked="checked"{/if}>
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Default Status</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 span2 select100" name="salesorder_map_default_status">
                                                            {foreach key = left item=value from = $salesOrderStatusVT}
                                                                <option value="{$value.value}" {if $salesorder_map_default_status eq $value.value} selected {/if}> {$value.label} </option>
                                                            {/foreach}
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>

                                <table class="table table-bordered equalSplit detailview-table">
                                    <thead>
                                        <tr>
                                            <th class="blockHeader" colspan="4">Invoice Sync Configuration</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Invoice Sync Interval</label>
                                            </td>
                                            <td>
                                                <div class="input-append row-fluid">
                                                    <div class="span10 row-fluid date">
                                                        <input type="checkbox" name="invoice_syncinterval" {if $invoice_syncinterval eq "on" }checked="checked"{/if}>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>


                                    </tbody>


                                </table>

                                <table class="table table-bordered equalSplit detailview-table">
                                    <thead>
                                        <tr>
                                            <th class="blockHeader" colspan="4">Customer Account Sync Configuration</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Invoice Sending Method</label>
                                            </td>
                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 select100 span2" name="account_invoice_method">
                                                            {foreach key = left item=value from = $sendingMethod}
                                                                <option value="{$value.value}" {if $account_invoice_method eq $value.value} selected {/if}> {$value.label} </option>
                                                            {/foreach}
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table table-bordered equalSplit detailview-table">
                                    <thead>
                                        <tr>
                                            <th class="blockHeader" colspan="4">Product Sync Configuration</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Get Stock Count From EOL</label>
                                            </td>
                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <input type="checkbox" name="product_get_stock_count" {if $product_get_stock_count eq "on" }checked="checked"{/if}>
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Get Reorder Level From EOL</label>
                                            </td>
                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <input type="checkbox" name="product_get_reorder_level" {if $product_get_reorder_level eq "on" }checked="checked"{/if}>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table table-bordered equalSplit detailview-table">
                                    <thead>
                                        <tr>
                                            <th class="blockHeader" colspan="4">Product Mapping</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Description</label>
                                            </td>

                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 span2 select100" name="product_map_description">
                                                            {foreach key = left item=value from = $PRODUCTMODFIELDS}
                                                                {if $value.uitype eq 1 or $value.uitype eq 2 or $value.uitype eq 15 or $value.uitype eq 16 or $value.uitype eq 19}
                                                                    <option value="{$value.columnname}" {if $product_map_description eq $value.columnname} selected="selected" {/if}> {$value.fieldlabel} </option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>

                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Code</label>
                                            </td>

                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 span2 select100" name="product_map_code">
                                                            {foreach key = left item=value from = $PRODUCTMODFIELDS}
                                                                {if $value.uitype eq 1 or $value.uitype eq 2 or $value.uitype eq 15 or $value.uitype eq 16 or $value.uitype eq 19}
                                                                    <option value="{$value.columnname}" {if $product_map_code eq $value.columnname} selected="selected" {/if}> {$value.fieldlabel} </option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Extra Description</label>
                                            </td>

                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 span2 select100" name="product_map_extra_desc">
                                                            {foreach key = left item=value from = $PRODUCTMODFIELDS}
                                                                {if $value.uitype eq 1 or $value.uitype eq 2 or $value.uitype eq 15 or $value.uitype eq 16 or $value.uitype eq 19}
                                                                    <option value="{$value.columnname}" {if $product_map_extra_desc eq $value.columnname} selected="selected" {/if}> {$value.fieldlabel} </option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Pull Categories from EOL</label>
                                            </td>
                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <input type="checkbox" name="product_map_pull_categories_from_eol" {if $product_map_pull_categories_from_eol eq "on" }checked="checked"{/if}>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Push Categories to EOL</label>
                                            </td>
                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <input type="checkbox" name="product_map_push_categories_to_eol" {if $product_map_push_categories_to_eol eq "on" }checked="checked"{/if}>
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Default Category</label>
                                            </td>
                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        <select class="select2 span2 select100" name="product_map_default_category">
                                                            {foreach key = left item=value from = $productCategories}
                                                                <option value="{$value.value}" {if $product_map_default_category eq $value.value} selected {/if}> {$value.label} </option>
                                                            {/foreach}
                                                        </select>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table table-bordered equalSplit detailview-table">
                                    <thead>
                                        <tr>
                                            <th class="blockHeader" colspan="4">Users Mapping</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach key = index item=vtigerUser from = $vtigerUsers}
                                            <tr>
                                                <td class="fieldLabel medium">{$vtigerUser.user_name}</td>
                                                <td class="fieldValue medium">
                                                    <div class="row-fluid">
                                                        <span class="span10">
                                                            <select class="select2 span2 select100" name="user_map_{$vtigerUser.user_name}">
                                                                {foreach key = left item=value from = $eolUsers}
                                                                    <option value="{$value.id}" {if $user_map_{$vtigerUser.user_name} eq {$value.id}} selected {/if} > {$value.label} </option>
                                                                {/foreach}
                                                            </select>
                                                        </span>
                                                </td>
                                            </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                        </form>
                    </div>
                </div>				
            </div>
        </div>
    </div>
</div>
</div>
