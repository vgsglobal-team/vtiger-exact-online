<script>
    $(document).ready(function () {
        $('#datepicker').datepicker();
    });
</script>
<style>
    .equalSplit{
        margin-top:10px;
    }
</style>
<div class="bodyContents" style="min-height: 263px;padding:0 0;">
    <div class="mainContainer row-fluid" style="min-height: 550px;margin-top:0px;">
        <div id="leftPanel" class="span2 row-fluid " style="min-height:550px;">
            {include file="Sideb.tpl"|vtemplate_path:$MODULE}
        </div>

        <div id="rightPanel" class="contentsDiv span10 marginLeftZero" style="min-height:550px;">
            <div class="container-fluid">
                <div class="widget_header row-fluid">
                    <div class="span12">
                        <form id="eol-config" class="form-horizontal" data-detail-url="{$detail_url}" action="{$action_url}" method="post">

                            <div class="span12">
                                <div class="span4 marginLeftZero">
                                    <h3>{$MODULE} Configurations</h3>
                                </div>
                                <div class="span4 pull-right">
                                    <div> <a style="float:right;margin-right:3%;position:relative;margin-top:-1%;" class="btn" value="SAVE" href="{$editconfigurl}" ><strong>Edit</strong></a></div>
                                    <div> <a style="float:right;margin-right:3%;position:relative;margin-top:-1%;" class="btn" value="SAVE" href="{$paralyseurl}" ><strong>Paralyse</strong></a></div>
                                </div>
                            </div>

                            <div class="span12">
                                <table class="table table-bordered equalSplit detailview-table">
                                    <thead>
                                        <tr>
                                            <th class="blockHeader" colspan="4">SalesOrder Sync configuration</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Warehouse</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {foreach key = left item=value from = $WAREHOUSES}
                                                            {if $salesorder_warehouse eq $value.value} {$value.label} {/if}
                                                        {/foreach}
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Default VAT Code</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {foreach key = left item=value from = $VATCODES}
                                                            {if $salesorder_dafaultvatcode eq $value.value} {$value.label} {/if}
                                                        {/foreach}
                                                    </span>
                                                </div>
                                            </td>
                                        <tr>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Cost Center</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {foreach key = left item=value from = $COSTCENTERS}
                                                            {if $salesorder_costcenter eq $value.value} {$value.label} {/if}
                                                        {/foreach}
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px"> Sync SalesOrder</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {if $salesorder_syncstate eq "1"} Yes {/if}
                                                        {if $salesorder_syncstate eq "0"} No {/if}
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Get Status From EOL</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {if $salesorder_getstatus eq "1"} Yes {/if}
                                                        {if $salesorder_getstatus eq "0"} No {/if}
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table table-bordered equalSplit detailview-table">
                                    <thead>
                                        <tr>
                                            <th class="blockHeader" colspan="4">SalesOrder Mapping</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Description</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {foreach key = left item=value from = $salesOrderFields}
                                                            {if $value.uitype eq 1 or $value.uitype eq 2 or $value.uitype eq 15 or $value.uitype eq 16 or $value.uitype eq 19}
                                                                {if $salesorder_map_description eq $value.columnname} {$value.fieldlabel} {/if}
                                                            {/if}
                                                        {/foreach}
                                                    </span>
                                                </div>
                                            </td>

                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Order Number</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {foreach key = left item=value from = $salesOrderFields}
                                                            {if $value.uitype eq 1 or $value.uitype eq 2 or $value.uitype eq 15 or $value.uitype eq 16 or $value.uitype eq 19}
                                                                {if $salesorder_map_ordernumber eq $value.columnname}  {$value.fieldlabel}{/if} 
                                                            {/if}
                                                        {/foreach}
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Order Date</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {foreach key = left item=value from = $salesOrderFields}
                                                            {if $value.uitype eq 5}
                                                                {if $salesorder_map_orderdate eq $value.columnname}{$value.fieldlabel} {/if}
                                                            {/if}
                                                        {/foreach}
                                                    </span>
                                                </div>
                                            </td>

                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Delivery Date</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {foreach key = left item=value from = $salesOrderFields}
                                                            {if $value.uitype eq 5}
                                                                {if $salesorder_map_deliverdate eq $value.columnname} {$value.fieldlabel} {/if}
                                                            {/if}
                                                        {/foreach}
                                                    </span>
                                                </div>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Your Ref</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {foreach key = left item=value from = $salesOrderFields}
                                                            {if $value.uitype eq 1 or $value.uitype eq 2 or $value.uitype eq 15 or $value.uitype eq 16 or $value.uitype eq 19}
                                                                {if $salesorder_map_yourref eq $value.columnname}  {$value.fieldlabel}  {/if}
                                                            {/if}
                                                        {/foreach}
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Payment Condition</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {$salesorder_map_payment_condition}
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Create Values for PC</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {if $salesorder_map_cvalues4pc eq "on" }Yes{else}No{/if}
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Pull Status from EOL</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {if $salesorder_map_pullstatusfromeol eq "on" }Yes{else}No{/if}
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Push Status to EOL</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {if $salesorder_map_pushstatustoeol eq "on" }Yes{else}No{/if}
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Default Status</label>
                                            </td>
                                            <td>
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {$salesorder_map_default_status}
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table table-bordered equalSplit detailview-table">
                                    <thead>
                                        <tr>
                                            <th class="blockHeader" colspan="4">Invoice Sync Configuration</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Invoice Sync Interval</label>
                                            </td>
                                            <td>
                                                <div class="input-append row-fluid">
                                                    <div class="span10 row-fluid date">
                                                        {if $invoice_syncinterval eq "on" }Yes{else}No{/if}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>


                                    </tbody>


                                </table>

                                <table class="table table-bordered equalSplit detailview-table">
                                    <thead>
                                        <tr>
                                            <th class="blockHeader" colspan="4">Customer Account Sync Configuration</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Invoice Sending Method</label>
                                            </td>
                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {foreach key = left item=value from = $sendingMethod}
                                                            {if $account_invoice_method eq $value.value}  {$value.label}  {/if}
                                                        {/foreach}
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table table-bordered equalSplit detailview-table">
                                    <thead>
                                        <tr>
                                            <th class="blockHeader" colspan="4">Product Sync Configuration</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Get Stock Count From EOL</label>
                                            </td>
                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {if $product_get_stock_count eq "on" }Yes{else}No{/if}
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Get Reorder Level From EOL</label>
                                            </td>
                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {if $product_get_reorder_level eq "on" }Yes{else}No{/if}
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table table-bordered equalSplit detailview-table">
                                    <thead>
                                        <tr>
                                            <th class="blockHeader" colspan="4">Product Mapping</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Description</label>
                                            </td>

                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {foreach key = left item=value from = $PRODUCTMODFIELDS}
                                                            {if $value.uitype eq 1 or $value.uitype eq 2 or $value.uitype eq 15 or $value.uitype eq 16 or $value.uitype eq 19}
                                                                {if $product_map_description eq $value.columnname}  {$value.fieldlabel}  {/if}
                                                            {/if}
                                                        {/foreach}
                                                    </span>
                                                </div>
                                            </td>

                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Code</label>
                                            </td>

                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {foreach key = left item=value from = $PRODUCTMODFIELDS}
                                                            {if $value.uitype eq 1 or $value.uitype eq 2 or $value.uitype eq 15 or $value.uitype eq 16 or $value.uitype eq 19}
                                                                {if $product_map_code eq $value.columnname}  {$value.fieldlabel}{/if}
                                                            {/if}
                                                        {/foreach}
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Extra Description</label>
                                            </td>

                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {foreach key = left item=value from = $PRODUCTMODFIELDS}
                                                            {if $value.uitype eq 1 or $value.uitype eq 2 or $value.uitype eq 15 or $value.uitype eq 16 or $value.uitype eq 19}
                                                                {if $product_map_extra_desc eq $value.columnname}  {$value.fieldlabel} {/if}
                                                            {/if}
                                                        {/foreach}
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Pull Categories from EOL</label>
                                            </td>
                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {if $product_map_pull_categories_from_eol eq "on" }Yes{else}No{/if}
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Push Categories to EOL</label>
                                            </td>
                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {if $product_map_push_categories_to_eol eq "on" }Yes{else}No{/if}
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="fieldLabel medium">
                                                <label class="muted pull-right marginRight10px">Default Category</label>
                                            </td>
                                            <td class="fieldValue medium">
                                                <div class="row-fluid">
                                                    <span class="span10">
                                                        {$product_map_default_category}
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table table-bordered equalSplit detailview-table">
                                    <thead>
                                        <tr>
                                            <th class="blockHeader" colspan="4">Users Mapping</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach key = index item=vtigerUser from = $vtigerUsers}
                                            <tr>
                                                <td class="fieldLabel medium">{$vtigerUser.user_name}</td>
                                                <td class="fieldValue medium">
                                                    <div class="row-fluid">
                                                        <span class="span10">
                                                            {foreach key = left item=value from = $eolUsers}
                                                                {if $user_map_{$vtigerUser.user_name} eq {$value.id}}  {$value.label} {/if} 
                                                            {/foreach}
                                                        </span>
                                                </td>
                                            </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                        </form>
                    </div>
                </div>				
            </div>
        </div>
    </div>
</div>
</div>
